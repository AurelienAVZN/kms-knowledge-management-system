﻿namespace Controller.Core;

public static class SwaggerExtension
{
    public static void AddSwagger(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddOpenApiDocument(doc =>
        {
            doc.Version = configuration.GetSection("Version").Value;
            doc.Title = configuration.GetSection("AppName").Value;
            doc.Description = configuration.GetSection("Description").Value;
            doc.DocumentName = configuration.GetSection("AppName").Value;
        });
    }

    public static void UseSwagger(this IApplicationBuilder app)
    {
        app.UseOpenApi();
        app.UseSwaggerUi3(options =>
        {
            //options.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
        });
    }
}
