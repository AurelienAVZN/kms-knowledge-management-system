﻿namespace Kms.Core;

/// <summary>
/// 
/// </summary>
/// <typeparam name="T"></typeparam>
public interface IControllerBase<T, M>
{
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    IEnumerable<T> List();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="guid"></param>
    /// <param name="code"></param>
    /// <returns></returns>
    T Get(string guid, string code);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="obj"></param>
    public void Post(M obj);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="obj"></param>
    public void Put(M obj);
}
