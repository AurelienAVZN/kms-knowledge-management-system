﻿namespace Database.Core;

/// <summary>
/// Enueration of the different database take in charge by the application
/// </summary>
public enum Provider
{
    #region SQL Database
    /// <summary>
    /// Indicate that the database is MSSQL
    /// </summary>
    SqlServer,
    Oracle,
    Postgre,
    Sqlite,
    MySql,
    MariaDb,
    #endregion
    #region NoSql Database
    #endregion
}
