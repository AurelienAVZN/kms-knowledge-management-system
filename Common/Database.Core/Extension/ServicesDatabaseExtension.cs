﻿namespace Database.Core;

/// <summary>
/// Static class that contains extension method in order to simplify 
/// the database service installation
/// </summary>
public static class ServicesDatabaseExtension
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="services"></param>
    /// <param name="connectionStrings"></param>
    /// <param name="schema"></param>
    /// <exception cref="NotImplementedException"></exception>
    /// <exception cref="ArgumentException"></exception>
    public static void AddDbContext<T>(this IServiceCollection services, string connectionStrings, string? schema)
        where T: DbContext
    {
        switch (DatabaseInfoHelper.Provider)
        {
            case Provider.SqlServer:
                services.AddDbContext<T>(delegate (DbContextOptionsBuilder options)
                {
                    options.UseLazyLoadingProxies(true).UseSqlServer(connectionStrings,
                        delegate (SqlServerDbContextOptionsBuilder sqliteBuilder)
                        {
                            sqliteBuilder.MigrationsHistoryTable("__EFMigrationsHistory", schema);
                        });
                });
                break;
            case Provider.Oracle:
                //services.AddDbContext<T>(options => options.UseOracle(connectionStrings));
                throw new NotImplementedException("Not implement yet !");
            case Provider.Postgre:
                //services.AddDbContext<T>(options => options.UseNpgsql(connectionStrings));
                throw new NotImplementedException("Not implement yet !");
            case Provider.Sqlite:
                services.AddDbContext<T>(delegate (DbContextOptionsBuilder options)
                {
                    options.UseLazyLoadingProxies(true).UseSqlite(connectionStrings,
                        delegate (SqliteDbContextOptionsBuilder sqliteBuilder)
                        {
                            sqliteBuilder.MigrationsHistoryTable("__EFMigrationsHistory", "ViewerFaker");
                        });
                });
                break;
            case Provider.MySql:
                throw new NotImplementedException("Not implement yet !");
            case Provider.MariaDb:
                throw new NotImplementedException("Not implemented yet !");
            default:
                throw new ArgumentException("Provider not take in charge");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="serviceProvider"></param>
    public static void DoMigration<T>(this IServiceProvider serviceProvider) where T: DbContext
    {
        using (var scopee = serviceProvider.CreateScope())
        {
            var dataContext = scopee.ServiceProvider.GetRequiredService<T>();
            dataContext.Database.Migrate();
        }
    }
}
