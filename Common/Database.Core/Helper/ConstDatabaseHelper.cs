﻿namespace Database.Core;

/// <summary>
/// Class that contains all the constant specific for the database
/// </summary>
public static class ConstDatabaseHelper
{
    public static readonly Type stringType = typeof(string);
    public static readonly Type GuidType = typeof(Guid);
    public static readonly Type boolType = typeof(bool);
    public static readonly Type intType = typeof(int);
    public static readonly Type byteArrayType = typeof(byte[]);
    public static readonly Type dateTimeType = typeof(DateTime);
    public const string MSSQLSEPARATEUR = "@";
    public const string ORACLESEPARATEUR = ":";
}