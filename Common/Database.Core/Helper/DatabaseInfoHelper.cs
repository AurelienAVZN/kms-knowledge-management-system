﻿namespace Database.Core;

/// <summary>
/// Helper class that contains the basic information of the database that we using
/// for our api
/// </summary>
public static class DatabaseInfoHelper
{
    public static string? ConnectionStrings { get; private set; }

    public static Provider Provider { get; private set; }

    public static string? Schema { get; private set; }

    public static void SetDatabaseInfo(string connectionStrings, string databaseType)
    {
        if(!Enum.TryParse<Provider>(databaseType, out var provider))
        {
            throw new Exception("Provider unrecognized !");
        }
        Provider = provider;
        ConnectionStrings = connectionStrings;
        //TODO faire le traitement du connectionstrings
    }

    public static bool IsOracle() => Provider == Provider.Oracle;

    public static bool IsMssql() => Provider == Provider.SqlServer;

    public static bool IsSqlite() => Provider == Provider.Sqlite;

    public static bool IsMariaDb() => Provider == Provider.MariaDb;

    public static bool IsPostgre() => Provider == Provider.Postgre;

    public static bool IsMySql() => Provider == Provider.MySql;

    public static string GetDatabaseType(Type type)
    {
        return Provider switch
        {
            Provider.SqlServer => sqlServerType[type],
            Provider.Postgre => postgreType[type].ToString(),
            Provider.Oracle => oracleType[type],
            Provider.Sqlite => sqliteType[type],
            Provider.MySql or Provider.MariaDb => throw new NotImplementedException("Not take in charge yet !"),
            _ => throw new NotSupportedException("Provider not supported !"),
        };
    }

    #region Database Type
    internal static readonly Dictionary<Type, string> sqlServerType = new()
    {
        { ConstDatabaseHelper.GuidType, "UNIQUEIDENTIFIER" },
        { ConstDatabaseHelper.stringType, "VARCHAR" },
        { ConstDatabaseHelper.dateTimeType, "DATETIME" },
        { ConstDatabaseHelper.boolType, "BIT" },
        { ConstDatabaseHelper.intType, "INT" },
        { ConstDatabaseHelper.byteArrayType, "VARBINARY(MAX)" }
    };

    internal static readonly Dictionary<Type, string> oracleType = new()
    {
        { ConstDatabaseHelper.GuidType, "RAW(16)" },
        { ConstDatabaseHelper.stringType, "VARCHAR2" },
        { ConstDatabaseHelper.dateTimeType, "TIMESTAMP" },
        { ConstDatabaseHelper.boolType, "NUMBER(1)" },
        { ConstDatabaseHelper.intType, "NUMBER" },
        { ConstDatabaseHelper.byteArrayType, "BLOB" }
    };

    internal static readonly Dictionary<Type, NpgsqlDbType> postgreType = new()
    {
        { ConstDatabaseHelper.GuidType, NpgsqlDbType.Uuid },
        { ConstDatabaseHelper.stringType, NpgsqlDbType.Varchar },
        { ConstDatabaseHelper.dateTimeType, NpgsqlDbType.Timestamp },
        { ConstDatabaseHelper.boolType, NpgsqlDbType.Bit },
        { ConstDatabaseHelper.intType, NpgsqlDbType.Integer },
        { ConstDatabaseHelper.byteArrayType, NpgsqlDbType.Bytea }
    };

    internal static readonly Dictionary<Type, string> sqliteType = new()
    {
        { ConstDatabaseHelper.GuidType, "TEXT" },
        { ConstDatabaseHelper.stringType, "TEXT" },
        { ConstDatabaseHelper.dateTimeType, "TEXT" },
        { ConstDatabaseHelper.boolType, "INTEGER" },
        { ConstDatabaseHelper.intType, "INTEGER" },
        { ConstDatabaseHelper.byteArrayType, "TEXT" }

    };
    #endregion
}
