﻿namespace Database.Core;

/// <summary>
/// Helper class to simplify management of database provider in order to get object (connection, command, etc..)
/// </summary>
public static class DatabaseHelper
{
    #region GetDbConnection Methods
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public static DbConnection GetDbConnection()
        => GetDbConnection(DatabaseInfoHelper.Provider, DatabaseInfoHelper.ConnectionStrings!);



    /// <summary>
    /// Give a connection of the database in function of the provider given
    /// </summary>
    /// <param name="provider">Provider of the database</param>
    /// <param name="connectionString">Connectionstring of the database</param>
    /// <returns></returns>
    /// <exception cref="NotImplementedException">When provider is MySql or MariaDb</exception>
    /// <exception cref="NotSupportedException">Provider given is not supported by the application</exception>
    public static DbConnection GetDbConnection(Provider provider, string connectionString)
    => provider switch
    {
        Provider.SqlServer => new SqlConnection(connectionString),
        Provider.Oracle => new OracleConnection(connectionString),
        Provider.Postgre => new NpgsqlConnection(connectionString),
        Provider.Sqlite => new SqlConnection(connectionString),
        Provider.MariaDb => throw new NotImplementedException(),
        Provider.MySql => throw new NotImplementedException(),
        _ => throw new NotSupportedException(),
    };
    #endregion

    #region GetColumnMaxLength Methods
    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    /// <param name="type"></param>
    /// <param name="columnName"></param>
    /// <returns></returns>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="NullReferenceException"></exception>
    public static int GetColumnMaxLength(DbContext context, Type type, string columnName)
    {
        if(string.IsNullOrWhiteSpace(columnName))
            throw new ArgumentNullException(nameof(columnName));
        var entityType = context.Model.FindEntityType(type) ?? throw new NullReferenceException($"No entity type found aasociated to the type {type.Name}");
        var length = entityType.GetProperty(columnName).GetMaxLength();
        if (length is null or -1)
            return -1;
        return (int)length;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    /// <param name="type"></param>
    /// <param name="columnName"></param>
    /// <param name="length"></param>
    /// <returns></returns>
    public static bool TryGetColumnMaxLength(DbContext context, Type type, string columnName, out int length)
    {
        try
        {
            length = GetColumnMaxLength(context, type, columnName);
            return true;
        }
        catch 
        {
            length = -1;
            return false; 
        }
    }
    #endregion
}
