﻿namespace Database.Core;

/// <summary>
/// 
/// </summary>
public static class CreateEntityTable
{
    static readonly string typeGuid = DatabaseInfoHelper.GetDatabaseType(ConstDatabaseHelper.GuidType);
    static readonly string typeByteArray = DatabaseInfoHelper.GetDatabaseType(ConstDatabaseHelper.byteArrayType);
    static readonly string typeDate = DatabaseInfoHelper.GetDatabaseType(ConstDatabaseHelper.dateTimeType);
    static readonly string typeBool = DatabaseInfoHelper.GetDatabaseType(ConstDatabaseHelper.boolType);
    static readonly string typeString = DatabaseInfoHelper.GetDatabaseType(ConstDatabaseHelper.stringType);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="migrationBuilder"></param>
    public static void CreatePictureBlob(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "PICTUREBLOB",
            comment: "Contains an image of an entity in order to recognize them",
            columns: table => new
            {
                ID = table.Column<Guid>(type: typeGuid, nullable: false, defaultValue: Guid.NewGuid(), comment: "Identifier of the picture"),
                CONTENT = table.Column<byte[]>(type: typeByteArray, nullable: false, comment: "The picture"),
                CREATIONDATE = table.Column<DateTime>(type: typeDate, nullable: false, defaultValue: DateTime.Now, comment: "Date when the data was inserted")
            },
            constraints: table =>
            {
                table.PrimaryKey("PK_PICTUREBLOB", x => x.ID);
            }
        );
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="migrationBuilder"></param>
    public static void CreateEntity(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.CreateTable(
            name: "ENTITY",
            comment: "Represent a person or an entity in the system",
            columns: table => new
            {
                ID = table.Column<Guid>(type: typeGuid, nullable: false, defaultValue: Guid.NewGuid(), comment: "Database identifier of the entity"),
                CODE = table.Column<string>(type: typeString, nullable: false, maxLength: 4, comment: "Identifier of the entity"),
                NAME = table.Column<string>(type: typeString, nullable: false, maxLength: 30, comment: "Identifier of the entity"),
                FAMILLYNAME = table.Column<string>(type: typeString, nullable: true, maxLength: 30, comment: "If the entity is a person, his family name"),
                MAIL = table.Column<string>(type: typeString, nullable: false, maxLength: 100, comment: "The mail address where to contact the entity"),
                PHONE = table.Column<string>(type: typeString, nullable: true, maxLength: 20, comment: "The phone number where to contact the entity"),
                PICTUREBLOBID = table.Column<Guid>(type: typeGuid, nullable: true, defaultValue: null, comment: "The phone number where to contact the entity"),
                CREATIONDATE = table.Column<DateTime>(type: typeDate, nullable: false, defaultValue: DateTime.Now, comment: "Date when the data was inserted"),
                UPDATEDATE = table.Column<DateTime>(type: typeDate, nullable: true, defaultValue: null, comment: "Date of the last update"),
                ISEMPLOYEE = table.Column<bool>(type: typeBool, nullable: false, defaultValue: true, comment: "Indicate if the entity is a employee of the company"),
                ISTRAINER = table.Column<bool>(type: typeBool, nullable: false, defaultValue: false, comment: "Indicate if the entity is a trainer")
            },
            constraints: table =>
            {
                table.PrimaryKey("PK_ENTITY", x => x.ID);
                table.ForeignKey("FK_ENTITY_PICTUREBLOB", x => x.PICTUREBLOBID, "PICTUREBLOB", "ID", onDelete: ReferentialAction.Cascade);
                table.UniqueConstraint("C1_ENTITY_UNIQUECODE", x => x.CODE);
                table.UniqueConstraint("C2_ENTITY_UNIQUEMAIL", x => x.MAIL);
            }
        );
        migrationBuilder.CreateIndex("ID1_ENTITY_NAME", "ENTITY", "NAME");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="migrationBuilder"></param>
    public static void Drop(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.DropIndex("ID1_ENTITY_NAME");
        migrationBuilder.DropTable("ENTITY");
        migrationBuilder.DropTable("PICTUREBLOB");
    }
}
