﻿namespace Database.Core;

/// <summary>
/// Contract that describes methods for the batabase object
/// </summary>
public interface IBaseObject
{
    /// <summary>
    /// Action to launch before saving the update or creation of the object
    /// in the database
    /// </summary>
    void BeforeSave();
}