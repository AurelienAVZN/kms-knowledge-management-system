﻿namespace Database.Core;

/// <summary>
/// 
/// </summary>
public interface IDbMigration
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="migrationBuilder"></param>
    void Up(MigrationBuilder migrationBuilder);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="migrationBuilder"></param>
    void Down(MigrationBuilder migrationBuilder);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="migrationBuilder"></param>
    void Triggers(MigrationBuilder migrationBuilder);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="migrationBuilder"></param>
    void Indexes(MigrationBuilder migrationBuilder);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="migrationBuilder"></param>
    void Tables(MigrationBuilder migrationBuilder);

}
