﻿namespace Database.Core;

/// <summary>
/// Contract that describes the minimal needed for a database service in order to work
/// </summary>
/// <typeparam name="T">The context type associated to the service</typeparam>
/// <see cref="IDisposable"/>
/// <see cref="DbContext"/>
public interface IDbService<T>: IDisposable where T: DbContext
{

    /// <summary>
    /// The database context associated to the service
    /// </summary>
    T Context { get; }

    /// <summary>
    /// Return a boolean that indicates if the code already exist in database or not
    /// </summary>
    /// <param name="jobCode">The code to check</param>
    /// <returns>Tru if the code is already present in the database, False else</returns>
    bool CheckCodeExisting<M>(string code) where M : class, ICode
        => Context.Set<M>().Any(x => x.Code == code);

    /// <summary>
    /// Retrieve an object by its code
    /// </summary>
    /// <typeparam name="T">The object retrive by the query if found</typeparam>
    /// <param name="context">Database context used to retrieve the object</param>
    /// <param name="code">The code </param>
    /// <returns></returns>
    public M? GetByCode<M>(string code) where M : class, ICode
        => Context.Set<M>().FirstOrDefault(x => x.Code == code);

    /// <summary>
    /// Retrieves 
    /// </summary>
    /// <param name="type"></param>
    /// <param name="columnName"></param>
    /// <returns></returns>
    public int GetColumnMaxLength(Type type, string columnName)
        => DatabaseHelper.GetColumnMaxLength(Context, type, columnName);
}
