﻿namespace Database.Core;

/// <summary>
/// Contract that exposes the minimal properties for database object which have a code property
/// </summary>
public interface ICode
{
    /// <summary>
    /// Unique identifier in the database of the object
    /// </summary>
    Guid Id { get; }

    /// <summary>
    /// Identifier of the object in the application
    /// </summary>
    string Code { get; }
}