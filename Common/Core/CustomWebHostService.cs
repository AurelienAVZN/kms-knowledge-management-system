﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.WindowsServices;

namespace Core;

public class CustomWebHostService: WebHostService
{
    public CustomWebHostService(IWebHost host) : base(host) { }
}

public static class WebHostServiceExtensions
{
    public static void RunAsCustomService(this IWebHost host)
    {
        var webHostService = new CustomWebHostService(host);
        System.ServiceProcess.ServiceBase.Run(webHostService);
    }
}