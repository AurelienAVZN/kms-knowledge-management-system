﻿namespace Kms.Controller;

public interface IKmsDbService: IDbService<KmsContext>
{
    void CreateOrUpdateJob(JobForm form);
}
