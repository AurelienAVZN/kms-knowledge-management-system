﻿namespace Kms.Controller;

[ApiController]
[Route("[controller]")]
[OpenApiTag("Job", Description = "Manage the data of the job all the action linked to it")]
public class JobController(ILogger<JobController> logger, IKmsDbService dbService, IHttpContextAccessor httpContextAccessor) : ControllerBase
{
    private readonly IHttpContextAccessor _httpContextAccessor = httpContextAccessor;
    private readonly ILogger<JobController> _logger = logger;
    private readonly IKmsDbService _dbService = dbService;

    [HttpGet]
    [Route("checkCode")]
    [Description("Indicates if a code for a job is already present in the database")]
    [SwaggerResponse(HttpStatusCode.OK, typeof(bool), Description = "The request is treated successfully")]
    [SwaggerResponse(HttpStatusCode.BadRequest, typeof(string), Description = "The code given is null or empty")]
    [SwaggerResponse(HttpStatusCode.InternalServerError, typeof(string), Description = "An error occured during ")]
    public ActionResult<bool> CheckCodeExisting([FromQuery(Name = "code")] string code)
        => Ok(_dbService.CheckCodeExisting<Job>(code));

    [HttpPost]
    [Description("Create or update a job in database with the information given")]
    [SwaggerResponse(HttpStatusCode.OK, typeof(void), Description = "The job object is updated or created")]
    [SwaggerResponse(HttpStatusCode.BadRequest, typeof(string), Description = "The content is null or missing an information")]
    [SwaggerResponse(HttpStatusCode.InternalServerError, typeof(string), Description = "An error unknow arrived")]
    public void Post([FromBody] JobForm form)
    {
        _dbService.CreateOrUpdateJob(form);
    }
}
