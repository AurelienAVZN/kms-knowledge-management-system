﻿namespace Kms.Controller;

[ApiController]
[Route("[controller]")]
public class EmployeeController(ILogger<EmployeeController> logger, IKmsDbService dbService) : ControllerBase
{
    private readonly ILogger<EmployeeController> _logger = logger;
    private readonly IKmsDbService _dbService = dbService;

    [HttpGet]
    [Route("checkcode")]
    public ActionResult<bool> CheckCodeAvailable([FromQuery] string code)
    {
        return true;
    }

    [HttpGet]
    public EmployeeForm Get([FromQuery] string id, [FromQuery] string code)
    {
        throw new NotImplementedException();
    }

    [HttpGet("list")]
    public IEnumerable<EmployeeForm> List()
    {
        throw new NotImplementedException();
    }

    [HttpPost("create")]
    public void Post([FromBody] EmployeeForm obj)
    {
        var employee = new Entity()
        {
            Code = obj.Code,
            Familyname = obj.FamilyName,
            Name = obj.Name,
            Pictureblobid = null,
            Mail = obj.Mail
        };
        //_context.Entities.Add(employee);
        //_context.SaveChanges();
    }

    [HttpPut("update")]
    public void Put([FromBody] EmployeeForm obj)
    {
        throw new NotImplementedException();
    }
}
