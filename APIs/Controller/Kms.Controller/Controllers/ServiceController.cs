﻿namespace Kms.Controller;

[ApiController]
[Route("[controller]")]
public class ServiceController(ILogger<ServiceController> logger, KmsContext context) : ControllerBase
{
    private readonly ILogger<ServiceController> _logger = logger;
    private readonly KmsContext _context = context;

    [HttpGet]
    [Description("Indicates if a code for a job is already present in the database")]
    public ActionResult<bool> CheckCodeExisting([FromQuery] string jobCode)
    {
        if (string.IsNullOrWhiteSpace(jobCode))
        {
            return BadRequest("The code given is null or empty");
        }
        return Ok(false);
    }
}
