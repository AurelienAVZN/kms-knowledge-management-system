﻿namespace Kms.Controller;

public class KmsDbService(ILogger<KmsDbService> logger, KmsContext context) : IKmsDbService
{
    private readonly ILogger<KmsDbService> _logger = logger;
    public KmsContext Context { get; private set; } = context;

    public void CreateOrUpdateJob(JobForm form)
    {
        bool found = true;
        var job = Context.Jobs.FirstOrDefault(x => x.Code == form.Code);
        if(job is null)
        {
            found = false;
            _logger.LogInformation($"No job found with code [{form.Code}]. Creation of the object...");
            job = new Job();
        }
        _logger.LogInformation($"Setting information for job {form.Code}");
        job.Label = form.Label;
        job.Description = job.Description;
        job.Startvaliditydate = form.StartValidityDate;
        job.Endvaliditydate = form.EndValidityDate;
        _ = !found ? Context.AddEntity(job) : Context.UpdateEntity(job);
        Context.SaveChanges();
    }

    public void Dispose()
    {
        Context.Dispose();
    }
}
