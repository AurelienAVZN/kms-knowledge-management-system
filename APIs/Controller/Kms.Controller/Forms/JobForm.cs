﻿namespace Kms.Controller;

public class JobForm
{
    public string Code { get; set; }

    public string? Label { get; set; }

    public string? Description { get; set; }

    public DateTime StartValidityDate { get; set; }

    public DateTime EndValidityDate { get; set; }
}
