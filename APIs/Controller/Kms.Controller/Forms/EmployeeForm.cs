﻿namespace Kms.Controller;

/// <summary>
/// 
/// </summary>
public class EmployeeForm
{
    public string Code { get; set; }

    public string Name { get; set; }

    public string? FamilyName { get; set; }

    public string Mail { get; set; }

    public string? Phone { get; set; }

    public byte[]? PictureBlob { get; set; }
}
