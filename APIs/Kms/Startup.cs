﻿using Controller.Core;
using Kms.Controller;

namespace Kms;

/// <summary>
/// 
/// </summary>
public class Startup
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="configuration">Configuration of the application</param>
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
        DatabaseInfoHelper.SetDatabaseInfo(Configuration.GetConnectionString("DefaultConnection")!, Configuration.GetRequiredSection("DatabaseType").Value!);
    }

    /// <summary>
    /// Configuration of the application
    /// </summary>
    public IConfiguration Configuration { get; }

    /// <summary>
    /// This method gets called by the runtime. Use this method to add services to the container.
    /// </summary>
    /// <param name="services">Collection services of the builder</param>
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddSerilog(config => { config.ReadFrom.Configuration(Configuration); });
        Log.Logger.Information("Ajout des services pour le webservice");
        services.AddLocalization(options => options.ResourcesPath = "Resources");
        services.AddHttpContextAccessor();
        services.AddRouting(options => options.LowercaseUrls = true);
        services.AddMvc();
        services.AddControllers();//.AddNewtonsoftJson();
        Log.Logger.Information("Ajout des services du swagger");
        services.AddSwagger(Configuration);
        Log.Logger.Information("Ajout du service de base de donnees");
        services.AddDbContext<KmsContext>(DatabaseInfoHelper.ConnectionStrings!, null);
        services.AddScoped<IKmsDbService, KmsDbService>();
        services.AddHealthChecks().AddCheck("self", () => HealthCheckResult.Healthy());
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    /// <summary>
    /// 
    /// </summary>
    /// <param name="app"></param>
    /// <param name="env"></param>
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
            app.UseDeveloperExceptionPage();
        app.UseHttpsRedirection();
        app.UseCors(corsBuilder =>
        {
            corsBuilder.AllowAnyOrigin();
            corsBuilder.AllowAnyMethod();
            corsBuilder.AllowAnyHeader();
        });
        app.UseRouting();
        app.UseAuthentication();
        app.UseAuthorization();
        app.UseEndpoints(endpoints =>{ endpoints.MapControllers(); });
        app.ApplicationServices.DoMigration<KmsContext>();
        app.UseSwagger();
        app.UseSerilogRequestLogging();
        app.UseHealthChecks("/", new HealthCheckOptions { Predicate = r => r.Name.Contains("self") });
    }
}
