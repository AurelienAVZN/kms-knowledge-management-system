namespace Kms;

/// <summary>
/// Lauching class of the Kms web service
/// </summary>
public class Program
{
    const string CONSOLE = "--console";

    /// <summary>
    /// 
    /// </summary>
    /// <param name="args">The arguments that comes from the command line which launches the web service</param>
    public static void Main(string[] args)
    {
        var isService = !(Debugger.IsAttached || args.Contains(CONSOLE));
        var pathToContentRoot = Environment.CurrentDirectory;
        if (isService)
        {
            pathToContentRoot = Path.GetDirectoryName(new Uri(AppDomain.CurrentDomain.BaseDirectory).LocalPath);
        }
        var host = BuildWebHost(args.Where(arg => arg != CONSOLE).ToArray(), pathToContentRoot!);
        if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows) && isService)
            host.RunAsCustomService();
        else
            host.Run();
    }

    /// <summary>
    /// Creating the builder for adding the services and launching the web service
    /// </summary>
    /// <param name="args">The arguments that comes from the command line which launches the web service</param>
    /// <param name="pathToContentRoot"></param>
    /// <returns></returns>
    public static IWebHost BuildWebHost(string[] args, string pathToContentRoot) => WebHost.CreateDefaultBuilder(args).UseContentRoot(pathToContentRoot)
        .ConfigureAppConfiguration((ctx, config) => 
        {
            foreach(var filpath in Directory.EnumerateFiles(".", "*.json", SearchOption.TopDirectoryOnly))
            {
                config.AddJsonFile(filpath, optional: false, reloadOnChange: false);
            }
        }).UseStartup<Startup>().UseKestrel().Build();
}