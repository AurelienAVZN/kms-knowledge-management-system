﻿namespace Kms.Database;

/// <summary>
/// Migration that creates the triggers for the different database
/// </summary>
//[DbContext(typeof(KmsContext))]
//[Migration("Kms_Migration_01")]
public class Migration_01 : Migration
{
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        var upgrader = Activator.CreateInstance(Type.GetType($"Migration_01_{DatabaseInfoHelper.Provider}")!) as IDbMigration;
        if (upgrader is null)
            throw new ApplicationException();
        upgrader.Up(migrationBuilder);
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
        var upgrader = Activator.CreateInstance(Type.GetType($"Migration_01_{DatabaseInfoHelper.Provider}")!) as IDbMigration;
        if (upgrader is null)
            throw new ApplicationException();
        upgrader.Down(migrationBuilder);
        base.Down(migrationBuilder);
    }
}