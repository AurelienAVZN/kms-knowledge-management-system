﻿namespace Kms.Database;

/// <summary>
/// Migration that creates the complete database for the API
/// </summary>
[DbContext(typeof(KmsContext))]
[Migration("Kms_Migration_00")]
public class Migration_00 : Migration
{
    readonly string typeGuid = DatabaseInfoHelper.GetDatabaseType(ConstDatabaseHelper.GuidType);
    readonly string typeDate = DatabaseInfoHelper.GetDatabaseType(ConstDatabaseHelper.dateTimeType);
    readonly string typeString = DatabaseInfoHelper.GetDatabaseType(ConstDatabaseHelper.stringType);
    readonly string typeInt = DatabaseInfoHelper.GetDatabaseType(ConstDatabaseHelper.intType);

    /// <inheritdoc/>
    protected override void Up(MigrationBuilder migrationBuilder)
    {
        CreateEntityTable.CreatePictureBlob(migrationBuilder);
        CreateEntityTable.CreateEntity(migrationBuilder);
        migrationBuilder.CreateTable(
            name: "ABSTRACTJOB",
            comment: "",
            columns: table => new
            {
                ID = table.Column<Guid>(type: typeGuid, nullable: false, defaultValue: Guid.NewGuid(), comment: "Database identifier of the job"),
                CODE = table.Column<string>(type: typeString, nullable: false, maxLength: 10, comment: "Identifier of the job"),
                LABEL = table.Column<string>(type: typeString, nullable: false, maxLength: 255),
                DESCRIPTION = table.Column<string>(type: typeString, nullable: false, maxLength: 4000),
                CREATIONDATE = table.Column<DateTime>(type: typeDate, nullable: false, defaultValue: DateTime.Now, comment: "Date when the data was inserted"),
                UPDATEDATE = table.Column<DateTime>(type: typeDate, nullable: true, defaultValue: null, comment: "Date of the last update")
            },
            constraints: table =>
            {
                table.PrimaryKey("PK_ABSTRACTJOB", x => x.ID);
                table.UniqueConstraint("C1_ABSTRACTJOB_UNIQUECODE", x => x.CODE);
            }
        );
        migrationBuilder.CreateTable(
           name: "SERVICE",
           comment: "",
           columns: table => new
           {
               ID = table.Column<Guid>(type: typeGuid, nullable: false, defaultValue: Guid.NewGuid(), comment: "Database identifier of the service"),
               CODE = table.Column<string>(type: typeString, nullable: false, maxLength: 10, comment: "Identifier of the service"),
               LABEL = table.Column<string>(type: typeString, nullable: false, maxLength: 255),
               DESCRIPTION = table.Column<string>(type: typeString, nullable: false, maxLength: 4000),
               CREATIONDATE = table.Column<DateTime>(type: typeDate, nullable: false, defaultValue: DateTime.Now, comment: "Date when the data was inserted"),
               UPDATEDATE = table.Column<DateTime>(type: typeDate, nullable: true, defaultValue: null, comment: "Date of the last update"),
               STARTVALIDITYDATE = table.Column<DateTime>(type: typeDate, nullable: true, defaultValue: null),
               ENDVALIDITYDATE = table.Column<DateTime>(type: typeDate, nullable: true, defaultValue: null)
           },
           constraints: table =>
           {
               table.PrimaryKey("PK_SERVICE", x => x.ID);
               table.UniqueConstraint("C1_SERVICE_UNIQUECODE", x => x.CODE);
           }
       );
        migrationBuilder.CreateTable(
            name: "JOB",
            comment: "",
            columns: table => new
            {
                ID = table.Column<Guid>(type: typeGuid, nullable: false, defaultValue: Guid.NewGuid(), comment: "Database identifier of the job"),
                ABSTRACTJOBID = table.Column<Guid>(typeGuid, nullable: true, defaultValue: null, comment: ""),
                SERVICEID = table.Column<Guid>(typeGuid, nullable: false, comment: ""),
                CODE = table.Column<string>(type: typeString, nullable: false, maxLength: 10, comment: "Identifier of the job"),
                LABEL = table.Column<string>(type: typeString, nullable: false, maxLength: 255),
                DESCRIPTION = table.Column<string>(type: typeString, nullable: false, maxLength: 4000),
                CREATIONDATE = table.Column<DateTime>(type: typeDate, nullable: false, defaultValue: DateTime.Now, comment: "Date when the data was inserted"),
                UPDATEDATE = table.Column<DateTime>(type: typeDate, nullable: true, defaultValue: null, comment: "Date of the last update"),
                STARTVALIDITYDATE = table.Column<DateTime>(type: typeDate, nullable: true, defaultValue: null),
                ENDVALIDITYDATE = table.Column<DateTime>(type: typeDate, nullable: true, defaultValue: null)
            },
            constraints: table =>
            {
                table.PrimaryKey("PK_JOB", x => x.ID);
                table.UniqueConstraint("C1_JOB_UNIQUECODE", x => x.CODE );
                table.ForeignKey("FK_JOB_SERVICE", x => x.SERVICEID, "SERVICE", "ID");
                table.ForeignKey("FK_JOB_ABSTRACTJOB", x => x.ABSTRACTJOBID, "ABSTRACTJOB", "ID");
            }
        );
        migrationBuilder.CreateTable(
            name: "AREA",
            comment: "",
            columns: table => new
            {
                ID = table.Column<Guid>(type: typeGuid, nullable: false, defaultValue: Guid.NewGuid(), comment: "Database identifier of the area"),
                CODE = table.Column<string>(type: typeString, nullable: false, maxLength: 10, comment: "Identifier of the area"),
                LABEL = table.Column<string>(type: typeString, nullable: false, maxLength: 255),
                DESCRIPTION = table.Column<string>(type: typeString, nullable: false, maxLength: 4000),
                CREATIONDATE = table.Column<DateTime>(type: typeDate, nullable: false, defaultValue: DateTime.Now, comment: "Date when the data was inserted"),
                UPDATEDATE = table.Column<DateTime>(type: typeDate, nullable: true, defaultValue: null, comment: "Date of the last update")
            },
            constraints: table =>
            {
                table.PrimaryKey("PK_AREA", x => x.ID);
            }
        );
        migrationBuilder.CreateTable(
            name: "SKILL",
            comment: "",
            columns: table => new
            {
                ID = table.Column<Guid>(type: typeGuid, nullable: false, defaultValue: Guid.NewGuid(), comment: "Database identifier of the skill"),
                CODE = table.Column<string>(type: typeString, nullable: false, maxLength: 10, comment: "Identifier of the skill"),
                LABEL = table.Column<string>(type: typeString, nullable: false, maxLength: 255),
                DESCRIPTION = table.Column<string>(type: typeString, nullable: false, maxLength: 4000),
                CREATIONDATE = table.Column<DateTime>(type: typeDate, nullable: false, defaultValue: DateTime.Now, comment: "Date when the data was inserted"),
                UPDATEDATE = table.Column<DateTime>(type: typeDate, nullable: true, defaultValue: null, comment: "Date of the last update")
            },
            constraints: table =>
            {
                table.PrimaryKey("PK_SKILL", x => x.ID);
                table.UniqueConstraint("C1_AREA_UNIQUECODE", x => x.CODE);
            }
        );
        migrationBuilder.CreateTable(
            name: "EMPLOYEESKILL",
            comment: "",
            columns: table => new
            {
                EMPLOYEEID = table.Column<Guid>(type: typeGuid, nullable: false, comment: "Identifier of the employee"),
                AREAID = table.Column<Guid>(type: typeGuid, nullable: false, comment: "Identifier of the area"),
                SKILLID = table.Column<Guid>(type: typeGuid, nullable: false, comment: "Identifier of the skill"),
                OBTAINEDDATE = table.Column<DateTime>(type: typeDate, nullable: false, defaultValue: DateTime.Now),
                LEVEL = table.Column<int>(type: typeInt, nullable: false, defaultValue: 0)
            },
            constraints: table =>
            {
                table.ForeignKey("FK_EMPLOYEESKILL_EMPLOYEE", x => x.EMPLOYEEID, "ENTITY", "ID", onDelete: ReferentialAction.Cascade);
                table.ForeignKey("FK_EMPLOYEESKILL_SKILL", x => x.SKILLID, "SKILL", "ID", onDelete: ReferentialAction.Cascade);
                table.ForeignKey("FK_EMPLOYEESKILL_AREA", x => x.AREAID, "AREA", "ID", onDelete: ReferentialAction.Cascade);
                table.CheckConstraint("C1_EMPLOYEESKILL_LEVEL", "LEVEL BETWEEN 0 AND 5");
            }
        );
        migrationBuilder.CreateTable(
            name: "JOBSKILL",
            comment: "",
            columns: table => new
            {
                JOBID = table.Column<Guid>(type: typeGuid, nullable: false, comment: "Identifier of the job"),
                AREAID = table.Column<Guid>(type: typeGuid, nullable: false, comment: "Identifier of the area"),
                SKILLID = table.Column<Guid>(type: typeGuid, nullable: false, comment: "Identifier of the skill"),
                STARTDATE = table.Column<DateTime>(type: typeDate, nullable: false, defaultValue: DateTime.Now),
                ENDDATE = table.Column<DateTime>(type: typeDate, nullable: true, defaultValue: null)
            },
            constraints: table =>
            {
                table.ForeignKey("FK_JOBSKILL_JOB", x => x.JOBID, "JOB", "ID", onDelete: ReferentialAction.Cascade);
                table.ForeignKey("FK_JOBSKILL_SKILL", x => x.SKILLID, "SKILL", "ID", onDelete: ReferentialAction.Cascade);
                table.ForeignKey("FK_JOBSKILL_AREA", x => x.AREAID, "AREA", "ID", onDelete: ReferentialAction.Cascade);
            }
        );
        migrationBuilder.CreateTable(
            name: "ABSTRACTJOBSKILL",
            comment: "",
            columns: table => new
            {
                ABSTRACTJOBID = table.Column<Guid>(type: typeGuid, nullable: false, comment: "Identifier of the job"),
                AREAID = table.Column<Guid>(type: typeGuid, nullable: false, comment: "Identifier of the area"),
                SKILLID = table.Column<Guid>(type: typeGuid, nullable: false, comment: "Identifier of the skill"),
                STARTDATE = table.Column<DateTime>(type: typeDate, nullable: false, defaultValue: DateTime.Now),
                ENDDATE = table.Column<DateTime>(type: typeDate, nullable: true, defaultValue: null)
            },
            constraints: table =>
            {
                table.ForeignKey("FK_ABSTRACTJOBSKILL_ABSTRACTJOB", x => x.ABSTRACTJOBID, "ABSTRACTJOB", "ID", onDelete: ReferentialAction.Cascade);
                table.ForeignKey("FK_ABSTRACTJOBSKILL_SKILL", x => x.SKILLID, "SKILL", "ID", onDelete: ReferentialAction.Cascade);
                table.ForeignKey("FK_ABSTRACTJOBSKILL_AREA", x => x.AREAID, "AREA", "ID", onDelete: ReferentialAction.Cascade);
            }
        );
        migrationBuilder.CreateTable(
            name: "EMPLOYEEJOBSERVICE",
            comment: "",
            columns: table => new
            {
                EMPLOYEEID = table.Column<Guid>(type: typeGuid, nullable: false, comment: "Identifier of the employee"),
                JOBID = table.Column<Guid>(type: typeGuid, nullable: false, comment: "Identifier of the job"),
                SERVICEID = table.Column<Guid>(type: typeGuid, nullable: false, comment: "Identifier of the service"),
                STARTDATE = table.Column<DateTime>(type: typeDate, nullable: false, defaultValue: DateTime.Now),
                ENDDATE = table.Column<DateTime>(type: typeDate, nullable: true, defaultValue: null)
            },
            constraints: table =>
            {
                table.ForeignKey("FK_EMPLOYEEJOBSERVICE_EMPLOYEE", x => x.EMPLOYEEID, "ENTITY", "ID", onDelete: ReferentialAction.Cascade);
                table.ForeignKey("FK_EMPLOYEEJOBSERVICE_JOB", x => x.SERVICEID, "JOB", "ID", onDelete: ReferentialAction.Cascade);
                table.ForeignKey("FK_EMPLOYEEJOBSERVICE_SERVICE", x => x.SERVICEID, "SERVICE", "ID", onDelete: ReferentialAction.Cascade);
            }
        );
        migrationBuilder.CreateTable(
            name: "SKILLAREA",
            comment: "",
            columns: table => new
            {
                AREAID = table.Column<Guid>(type: typeGuid, nullable: false, comment: "Identifier of the area"),
                SKILLID = table.Column<Guid>(type: typeGuid, nullable: false, comment: "Identifier of the skill"),
                STARTDATE = table.Column<DateTime>(type: typeDate, nullable: false, defaultValue: DateTime.Now),
                ENDDATE = table.Column<DateTime>(type: typeDate, nullable: true, defaultValue: null)
            },
            constraints: table =>
            {
                table.ForeignKey("FK_SKILLAREA_AREA", x => x.AREAID, "AREA", "ID", onDelete: ReferentialAction.Cascade);
                table.ForeignKey("FK_SKILLAREA_SKILL", x => x.SKILLID, "SKILL", "ID", onDelete: ReferentialAction.Cascade);
            }
        );
        migrationBuilder.CreateIndex("ID1_EMPLOYEEJOBSERVICE_STARTDATE", "EMPLOYEEJOBSERVICE", "STARTDATE");
        migrationBuilder.CreateIndex("ID1_EMPLOYEESKILL_LEVEL", "EMPLOYEESKILL", "LEVEL");
    }

    /// <inheritdoc/>
    protected override void Down(MigrationBuilder migrationBuilder)
    {
        migrationBuilder.DropIndex("ID1_EMPLOYEEJOBSERVICE_STARTDATE", "EMPLOYEEJOBSERVICE");
        migrationBuilder.DropIndex("ID1_EMPLOYEESKILL_LEVEL", "LEVEL");
        CreateEntityTable.Drop(migrationBuilder);
        //TODO faire le reste
        base.Down(migrationBuilder);
    }
}
