﻿namespace Kms.Database;

public partial class Pictureblob
{
    public Guid Id { get; set; }

    public byte[] Content { get; set; } = null!;

    public DateTime? Creationdate { get; set; }

    public virtual ICollection<Entity> Entities { get; set; } = new List<Entity>();
}
