﻿namespace Kms.Database;

public partial class Area
{
    public Guid Id { get; set; }

    public string Code { get; set; } = null!;

    public string? Label { get; set; }

    public string? Description { get; set; }

    public DateTime Creationdate { get; set; }

    public DateTime? Updatedate { get; set; }
}
