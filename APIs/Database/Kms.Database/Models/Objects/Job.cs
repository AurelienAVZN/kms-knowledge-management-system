﻿namespace Kms.Database;

public partial class Job
{
    public Guid Id { get; set; }

    public Guid Serviceid { get; set; }

    public Guid? Abstractjobid { get; set; }

    public string Code { get; set; } = null!;

    public string? Label { get; set; }

    public string? Description { get; set; }

    public DateTime Creationdate { get; set; }

    public DateTime? Updatedate { get; set; }

    public DateTime Startvaliditydate { get; set; }

    public DateTime Endvaliditydate { get; set; }

    public virtual Abstractjob? Abstractjob { get; set; }

    public virtual Service Service { get; set; } = null!;
}
