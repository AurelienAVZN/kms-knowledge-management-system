﻿namespace Kms.Database;

public partial class Entity
{
    public Guid Id { get; set; }

    public string Code { get; set; } = null!;

    public string Name { get; set; } = null!;

    public string? Familyname { get; set; }

    public string Mail { get; set; } = null!;

    public string? Phone { get; set; }

    public Guid? Pictureblobid { get; set; }

    public DateTime Creationdate { get; set; }

    public DateTime? Updatedate { get; set; }

    public bool? Isemployee { get; set; }

    public bool? Istrainer { get; set; }

    public virtual Pictureblob? Pictureblob { get; set; }
}
