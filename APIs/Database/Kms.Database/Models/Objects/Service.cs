﻿namespace Kms.Database;

public partial class Service
{
    public Guid Id { get; set; }

    public string Code { get; set; } = null!;

    public string? Label { get; set; }

    public string? Description { get; set; }

    public DateTime Creationdate { get; set; }

    public DateTime? Updatedate { get; set; }

    public DateTime Startvaliditydate { get; set; }

    public DateTime Endvaliditydate { get; set; }

    public virtual ICollection<Job> Jobs { get; set; } = new List<Job>();
}
