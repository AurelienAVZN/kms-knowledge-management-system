﻿namespace Kms.Database;

public partial class Job: ICode, IBaseObject
{
    public Job() 
    { 
        Creationdate = DateTime.Now;
        Startvaliditydate = DateTime.Now;
        Endvaliditydate = DateTime.MaxValue;
    }

    public void BeforeSave()
    {
        Updatedate = DateTime.Now;
        if(Startvaliditydate > Endvaliditydate)
        {
            //TODO Add a parameter to set a default value of end validity date
            Endvaliditydate = Startvaliditydate.AddYears(10);
        }
    }
}
