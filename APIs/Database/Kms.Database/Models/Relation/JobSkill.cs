﻿namespace Kms.Database;

public partial class Jobskill
{
    public Guid Jobid { get; set; }

    public Guid Skillid { get; set; }

    public Guid Areaid { get; set; }

    public DateTime Startdate { get; set; }

    public DateTime Enddate { get; set; }

    public virtual Area Area { get; set; } = null!;

    public virtual Job Job { get; set; } = null!;

    public virtual Skill Skill { get; set; } = null!;
}
