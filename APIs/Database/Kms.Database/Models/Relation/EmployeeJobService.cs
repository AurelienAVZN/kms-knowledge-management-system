﻿namespace Kms.Database;

public partial class Employeejobservice
{
    public Guid Employeeid { get; set; }

    public Guid Jobid { get; set; }

    public Guid Serviceid { get; set; }

    public DateTime Startdate { get; set; }

    public DateTime Enddate { get; set; }

    public virtual Entity Employee { get; set; } = null!;

    public virtual Job Job { get; set; } = null!;

    public virtual Service Service { get; set; } = null!;
}
