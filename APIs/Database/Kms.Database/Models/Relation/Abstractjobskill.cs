﻿namespace Kms.Database;

public partial class Abstractjobskill
{
    public Guid Skillid { get; set; }

    public Guid Abstractjobid { get; set; }

    public Guid Areaid { get; set; }

    public DateTime Startdate { get; set; }

    public DateTime Enddate { get; set; }

    public virtual Abstractjob Abstractjob { get; set; } = null!;

    public virtual Area Area { get; set; } = null!;

    public virtual Skill Skill { get; set; } = null!;
}
