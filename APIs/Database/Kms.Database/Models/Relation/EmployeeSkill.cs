﻿namespace Kms.Database;

public partial class Employeeskill
{
    public Guid Employeeid { get; set; }

    public Guid Skillid { get; set; }

    public Guid Areaid { get; set; }

    public DateTime Obtaineddate { get; set; }

    public int? Level { get; set; }

    public virtual Area Area { get; set; } = null!;

    public virtual Entity Employee { get; set; } = null!;

    public virtual Skill Skill { get; set; } = null!;
}
