﻿namespace Kms.Database;
public partial class Skillarea
{
    public Guid Skillid { get; set; }

    public Guid Areaid { get; set; }

    public DateTime Startdate { get; set; }

    public DateTime Enddate { get; set; }

    public virtual Area Area { get; set; } = null!;

    public virtual Skill Skill { get; set; } = null!;
}
