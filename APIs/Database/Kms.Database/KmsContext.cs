﻿namespace Kms.Database;

public partial class KmsContext : DbContext
{
    public KmsContext()
    {
    }

    public KmsContext(DbContextOptions<KmsContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Abstractjob> Abstractjobs { get; set; }

    public virtual DbSet<Abstractjobskill> Abstractjobskills { get; set; }

    public virtual DbSet<Area> Areas { get; set; }

    public virtual DbSet<Employeejobservice> Employeejobservices { get; set; }

    public virtual DbSet<Employeeskill> Employeeskills { get; set; }

    public virtual DbSet<Entity> Entities { get; set; }

    public virtual DbSet<Job> Jobs { get; set; }

    public virtual DbSet<Jobskill> Jobskills { get; set; }

    public virtual DbSet<Pictureblob> Pictureblobs { get; set; }

    public virtual DbSet<Service> Services { get; set; }

    public virtual DbSet<Skill> Skills { get; set; }

    public virtual DbSet<Skillarea> Skillareas { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Abstractjob>(entity =>
        {
            entity.ToTable("ABSTRACTJOB");
            entity.HasIndex(e => e.Code, "C1_ABSTRACTJOB_UNIQUECODE").IsUnique();
            entity.Property(e => e.Id).HasDefaultValue(Guid.NewGuid()).HasColumnName("ID");
            entity.Property(e => e.Code).HasMaxLength(10).IsUnicode(false).HasColumnName("CODE");
            entity.Property(e => e.Creationdate).HasDefaultValue(DateTime.Now).HasColumnName("CREATIONDATE");
            entity.Property(e => e.Description).HasMaxLength(4000).IsUnicode(false).HasDefaultValue(null).HasColumnName("DESCRIPTION");
            entity.Property(e => e.Label).HasMaxLength(255).IsUnicode(false).HasDefaultValue(null).HasColumnName("LABEL");
            entity.Property(e => e.Updatedate).HasDefaultValue(null).HasColumnName("UPDATEDATE");
        });

        modelBuilder.Entity<Abstractjobskill>(entity =>
        {
            entity.HasNoKey().ToTable("ABSTRACTJOBSKILL");
            entity.Property(e => e.Abstractjobid).HasColumnName("ABSTRACTJOBID");
            entity.Property(e => e.Areaid).HasColumnName("AREAID");
            entity.Property(e => e.Enddate).HasColumnName("ENDDATE");
            entity.Property(e => e.Skillid).HasColumnName("SKILLID");
            entity.Property(e => e.Startdate).HasDefaultValue(DateTime.Now).HasColumnName("STARTDATE");
            entity.HasOne(d => d.Abstractjob).WithMany().HasForeignKey(d => d.Abstractjobid).OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_ABSTRACTJOBSKILL_ABSTRACTJOB");
            entity.HasOne(d => d.Area).WithMany().HasForeignKey(d => d.Areaid).OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_ABSTRACTJOBSKILL_AREA");
            entity.HasOne(d => d.Skill).WithMany().HasForeignKey(d => d.Skillid).OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_ABSTRACTJOBSKILL_SKILL");
        });

        modelBuilder.Entity<Area>(entity =>
        {
            entity.ToTable("AREA");
            entity.HasIndex(e => e.Code, "C1_AREA_UNIQUECODE").IsUnique();
            entity.Property(e => e.Id).HasDefaultValue(Guid.NewGuid()).HasColumnName("ID");
            entity.Property(e => e.Code).HasMaxLength(10).IsUnicode(false).HasColumnName("CODE");
            entity.Property(e => e.Creationdate).HasDefaultValue(DateTime.Now).HasColumnName("CREATIONDATE");
            entity.Property(e => e.Description).HasMaxLength(4000).IsUnicode(false).HasDefaultValue(null).HasColumnName("DESCRIPTION");
            entity.Property(e => e.Label).HasMaxLength(255).IsUnicode(false).HasDefaultValue(null).HasColumnName("LABEL");
            entity.Property(e => e.Updatedate).HasDefaultValue(null).HasColumnName("UPDATEDATE");
        });

        modelBuilder.Entity<Employeejobservice>(entity =>
        {
            entity.HasNoKey().ToTable("EMPLOYEEJOBSERVICE");
            entity.Property(e => e.Employeeid).HasColumnName("EMPLOYEEID");
            entity.Property(e => e.Enddate).HasColumnName("ENDDATE");
            entity.Property(e => e.Jobid).HasColumnName("JOBID");
            entity.Property(e => e.Serviceid).HasColumnName("SERVICEID");
            entity.Property(e => e.Startdate).HasDefaultValue(DateTime.Now).HasColumnName("STARTDATE");
            entity.HasOne(d => d.Employee).WithMany().HasForeignKey(d => d.Employeeid).OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_EMPLOYEEJOBSERVICE_EMPLOYEE");
            entity.HasOne(d => d.Job).WithMany().HasForeignKey(d => d.Jobid).OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_EMPLOYEEJOBSERVICE_JOB");
            entity.HasOne(d => d.Service).WithMany().HasForeignKey(d => d.Serviceid).OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_EMPLOYEEJOBSERVICE_SERVICE");
        });

        modelBuilder.Entity<Employeeskill>(entity =>
        {
            entity.HasNoKey().ToTable("EMPLOYEESKILL");
            entity.Property(e => e.Areaid).HasColumnName("AREAID");
            entity.Property(e => e.Employeeid).HasColumnName("EMPLOYEEID");
            entity.Property(e => e.Level).HasDefaultValue(0).HasColumnName("LEVEL");
            entity.Property(e => e.Obtaineddate).HasDefaultValue(DateTime.Now).HasColumnName("OBTAINEDDATE");
            entity.Property(e => e.Skillid).HasColumnName("SKILLID");
            entity.HasOne(d => d.Area).WithMany().HasForeignKey(d => d.Areaid).OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_EMPLOYEESKILL_AREA");
            entity.HasOne(d => d.Employee).WithMany().HasForeignKey(d => d.Employeeid).OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_EMPLOYEESKILL_EMPLOYEE");
            entity.HasOne(d => d.Skill).WithMany().HasForeignKey(d => d.Skillid).OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_EMPLOYEESKILL_SKILL");
        });

        modelBuilder.Entity<Entity>(entity =>
        {
            entity.ToTable("ENTITY");
            entity.HasIndex(e => e.Code, "C1_ENTITY_UNIQUECODE").IsUnique();
            entity.HasIndex(e => e.Mail, "C2_ENTITY_UNIQUEMAIL").IsUnique();
            entity.Property(e => e.Id).HasDefaultValue(Guid.NewGuid()).HasColumnName("ID");
            entity.Property(e => e.Code).HasMaxLength(4).IsUnicode(false).HasColumnName("CODE");
            entity.Property(e => e.Creationdate).HasDefaultValue(DateTime.Now).HasColumnName("CREATIONDATE");
            entity.Property(e => e.Familyname).HasMaxLength(30).IsUnicode(false).HasDefaultValue(null).HasColumnName("FAMILYNAME");
            entity.Property(e => e.Isemployee).HasDefaultValue(true).HasColumnName("ISEMPLOYEE");
            entity.Property(e => e.Istrainer).HasDefaultValue(false).HasColumnName("ISTRAINER");
            entity.Property(e => e.Mail).HasMaxLength(100).IsUnicode(false).HasColumnName("MAIL");
            entity.Property(e => e.Name).HasMaxLength(30).IsUnicode(false).HasColumnName("NAME");
            entity.Property(e => e.Phone).HasMaxLength(20).IsUnicode(false).HasDefaultValue(null).HasColumnName("PHONE");
            entity.Property(e => e.Pictureblobid).HasDefaultValue(null).HasColumnName("PICTUREBLOBID");
            entity.Property(e => e.Updatedate).HasDefaultValue(null).HasColumnName("UPDATEDATE");
            entity.HasOne(d => d.Pictureblob).WithMany(p => p.Entities).HasForeignKey(d => d.Pictureblobid).HasConstraintName("FK_ENTITY_PICTUREBLOB");
        });

        modelBuilder.Entity<Job>(entity =>
        {
            entity.ToTable("JOB");
            entity.HasIndex(e => e.Code, "C1_JOB_UNIQUECODE").IsUnique();
            entity.Property(e => e.Id).HasDefaultValue(Guid.NewGuid()).HasColumnName("ID");
            entity.Property(e => e.Abstractjobid).HasDefaultValue(null).HasColumnName("ABSTRACTJOBID");
            entity.Property(e => e.Code).HasMaxLength(10).IsUnicode(false).HasColumnName("CODE");
            entity.Property(e => e.Creationdate).HasDefaultValue(DateTime.Now).HasColumnName("CREATIONDATE");
            entity.Property(e => e.Description).HasMaxLength(4000).IsUnicode(false).HasDefaultValue(null).HasColumnName("DESCRIPTION");
            entity.Property(e => e.Endvaliditydate).HasColumnName("ENDVALIDITYDATE");
            entity.Property(e => e.Label).HasMaxLength(255).IsUnicode(false).HasDefaultValue(null).HasColumnName("LABEL");
            entity.Property(e => e.Serviceid).HasColumnName("SERVICEID");
            entity.Property(e => e.Startvaliditydate).HasDefaultValue(DateTime.Now).HasColumnName("STARTVALIDITYDATE");
            entity.Property(e => e.Updatedate).HasDefaultValue(null).HasColumnName("UPDATEDATE");
            entity.HasOne(d => d.Abstractjob).WithMany(p => p.Jobs).HasForeignKey(d => d.Abstractjobid).HasConstraintName("FK_JOB_ABSTRACTJOB");
            entity.HasOne(d => d.Service).WithMany(p => p.Jobs).HasForeignKey(d => d.Serviceid).OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_JOB_SERVICE");
        });

        modelBuilder.Entity<Jobskill>(entity =>
        {
            entity.HasNoKey().ToTable("JOBSKILL");
            entity.Property(e => e.Areaid).HasColumnName("AREAID");
            entity.Property(e => e.Enddate).HasColumnName("ENDDATE");
            entity.Property(e => e.Jobid).HasColumnName("JOBID");
            entity.Property(e => e.Skillid).HasColumnName("SKILLID");
            entity.Property(e => e.Startdate).HasDefaultValue(DateTime.Now).HasColumnName("STARTDATE");
            entity.HasOne(d => d.Area).WithMany().HasForeignKey(d => d.Areaid).OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_JOBSKILL_AREA");
            entity.HasOne(d => d.Job).WithMany().HasForeignKey(d => d.Jobid).OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_JOBSKILL_JOB");
            entity.HasOne(d => d.Skill).WithMany().HasForeignKey(d => d.Skillid).OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_JOBSKILL_SKILL");
        });

        modelBuilder.Entity<Pictureblob>(entity =>
        {
            entity.ToTable("PICTUREBLOB");
            entity.Property(e => e.Id).HasDefaultValue(Guid.NewGuid()).HasColumnName("ID");
            entity.Property(e => e.Content).HasColumnName("CONTENT");
            entity.Property(e => e.Creationdate).HasDefaultValue(DateTime.Now).HasColumnType("datetime").HasColumnName("CREATIONDATE");
        });

        modelBuilder.Entity<Service>(entity =>
        {
            entity.ToTable("SERVICE");
            entity.HasIndex(e => e.Code, "C1_SERVICE_UNIQUECODE").IsUnique();
            entity.Property(e => e.Id).HasDefaultValue(Guid.NewGuid()).HasColumnName("ID");
            entity.Property(e => e.Code).HasMaxLength(10).IsUnicode(false).HasColumnName("CODE");
            entity.Property(e => e.Creationdate).HasDefaultValue(DateTime.Now).HasColumnName("CREATIONDATE");
            entity.Property(e => e.Description).HasMaxLength(4000).IsUnicode(false).HasDefaultValue(null).HasColumnName("DESCRIPTION");
            entity.Property(e => e.Endvaliditydate).HasColumnName("ENDVALIDITYDATE");
            entity.Property(e => e.Label).HasMaxLength(255).IsUnicode(false).HasDefaultValue(null).HasColumnName("LABEL");
            entity.Property(e => e.Startvaliditydate).HasDefaultValue(DateTime.Now).HasColumnName("STARTVALIDITYDATE");
            entity.Property(e => e.Updatedate).HasDefaultValue(null).HasColumnName("UPDATEDATE");
        });

        modelBuilder.Entity<Skill>(entity =>
        {
            entity.ToTable("SKILL");
            entity.HasIndex(e => e.Code, "C1_SKILL_UNIQUECODE").IsUnique();
            entity.Property(e => e.Id).HasDefaultValue(Guid.NewGuid()).HasColumnName("ID");
            entity.Property(e => e.Code).HasMaxLength(10).IsUnicode(false).HasColumnName("CODE");
            entity.Property(e => e.Creationdate).HasDefaultValue(DateTime.Now).HasColumnName("CREATIONDATE");
            entity.Property(e => e.Description).HasMaxLength(4000).IsUnicode(false).HasDefaultValue(null) .HasColumnName("DESCRIPTION");
            entity.Property(e => e.Label).HasMaxLength(255).IsUnicode(false).HasDefaultValue(null).HasColumnName("LABEL");
            entity.Property(e => e.Updatedate).HasDefaultValue(null).HasColumnName("UPDATEDATE");
        });

        modelBuilder.Entity<Skillarea>(entity =>
        {
            entity.HasNoKey().ToTable("SKILLAREA");
            entity.Property(e => e.Areaid).HasColumnName("AREAID");
            entity.Property(e => e.Enddate).HasColumnName("ENDDATE");
            entity.Property(e => e.Skillid).HasColumnName("SKILLID");
            entity.Property(e => e.Startdate).HasDefaultValue(DateTime.Now).HasColumnName("STARTDATE");
            entity.HasOne(d => d.Area).WithMany().HasForeignKey(d => d.Areaid).OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_SKILLAREA_AREA");
            entity.HasOne(d => d.Skill).WithMany().HasForeignKey(d => d.Skillid).OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_SKILLAREA_SKILL");
        });
        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
