﻿using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Kms.Database;

public partial class KmsContext
{
    private readonly List<Action> _actionsBeforeSave = [];

    public EntityEntry<T> AddEntity<T>(T entity) where T : class, IBaseObject
    {
        _actionsBeforeSave.Add(entity.BeforeSave);
        return Add(entity);
    }

    public EntityEntry<T> UpdateEntity<T>(T entity) where T : class, IBaseObject
    {        
        _actionsBeforeSave.Add(entity.BeforeSave);
        return Add(entity);
    }

    public override int SaveChanges()
    {
        foreach (var action in _actionsBeforeSave)
        {
            action.Invoke();
        }
        _actionsBeforeSave.Clear();
        return base.SaveChanges();
    }
}
