﻿namespace Dms.Database;

public enum DocumentHistoryType
{
    History = 0,
    Version = 1
}
