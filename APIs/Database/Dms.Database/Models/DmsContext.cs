﻿namespace Dms.Database;

public partial class DmsContext : DbContext
{
    public DmsContext()
    {
    }

    public DmsContext(DbContextOptions<DmsContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Document> Documents { get; set; }

    public virtual DbSet<Documentaudiovideoblob> Documentaudiovideoblobs { get; set; }

    public virtual DbSet<Documentaudiovideohistoryblob> Documentaudiovideohistoryblobs { get; set; }

    public virtual DbSet<Documentauthor> Documentauthors { get; set; }

    public virtual DbSet<Documentdiversblob> Documentdiversblobs { get; set; }

    public virtual DbSet<Documentdivershistoryblob> Documentdivershistoryblobs { get; set; }

    public virtual DbSet<Documenthistory> Documenthistories { get; set; }

    public virtual DbSet<Documentpictureblob> Documentpictureblobs { get; set; }

    public virtual DbSet<Documentpicturehistoryblob> Documentpicturehistoryblobs { get; set; }

    public virtual DbSet<Documenttag> Documenttags { get; set; }

    public virtual DbSet<Documenttextblob> Documenttextblobs { get; set; }

    public virtual DbSet<Documenttexthistoryblob> Documenttexthistoryblobs { get; set; }

    public virtual DbSet<Documentvalidation> Documentvalidations { get; set; }

    public virtual DbSet<Entity> Entities { get; set; }

    public virtual DbSet<Folder> Folders { get; set; }

    public virtual DbSet<Pictureblob> Pictureblobs { get; set; }

    public virtual DbSet<Repository> Repositories { get; set; }

    public virtual DbSet<Tag> Tags { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Document>(entity =>
        {
            entity.ToTable("DOCUMENT");
            entity.HasIndex(e => new { e.Label, e.Folderid }, "C1_DOCUMENT_UNIQUELABELFOLDER").IsUnique();
            entity.Property(e => e.Id).HasDefaultValue(Guid.NewGuid()).HasColumnName("ID");
            entity.Property(e => e.Audiovideoid).HasDefaultValue(null).HasColumnName("AUDIOVIDEOID");
            entity.Property(e => e.Creationdate).HasDefaultValue(DateTime.Now).HasColumnName("CREATIONDATE");
            entity.Property(e => e.Description).HasMaxLength(4000).IsUnicode(false).HasDefaultValue(null).HasColumnName("DESCRIPTION");
            entity.Property(e => e.Diversid).HasDefaultValue(null).HasColumnName("DIVERSID");
            entity.Property(e => e.Folderid).HasColumnName("FOLDERID");
            entity.Property(e => e.Format).HasMaxLength(10).IsUnicode(false).HasDefaultValue(null).HasColumnName("FORMAT");
            entity.Property(e => e.Isinvalid).HasColumnName("ISINVALID");
            entity.Property(e => e.Label).HasMaxLength(255).IsUnicode(false).HasColumnName("LABEL");
            entity.Property(e => e.Pictureid).HasDefaultValue(null).HasColumnName("PICTUREID");
            entity.Property(e => e.Stockage).HasMaxLength(4000).IsUnicode(false).HasDefaultValue(null).HasColumnName("STOCKAGE");
            entity.Property(e => e.Textid).HasDefaultValue(null).HasColumnName("TEXTID");
            entity.Property(e => e.Type).HasMaxLength(20).IsUnicode(false).HasDefaultValue("Divers").HasColumnName("TYPE");
            entity.Property(e => e.Updatedate).HasDefaultValue(null).HasColumnName("UPDATEDATE");
            entity.Property(e => e.Version).HasMaxLength(20).IsUnicode(false).HasDefaultValue(null).HasColumnName("VERSION");
            entity.HasOne(d => d.Audiovideo).WithMany(p => p.Documents).HasForeignKey(d => d.Audiovideoid).HasConstraintName("FK_DOCUMENT_DOCUMENTAUDIOVIDEOBLOB");
            entity.HasOne(d => d.Divers).WithMany(p => p.Documents).HasForeignKey(d => d.Diversid).HasConstraintName("FK_DOCUMENT_DOCUMENTDIVERSBLOB");
            entity.HasOne(d => d.Folder).WithMany(p => p.Documents).HasForeignKey(d => d.Folderid).OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_DOCUMENT_FOLDER");
            entity.HasOne(d => d.Picture).WithMany(p => p.Documents).HasForeignKey(d => d.Pictureid).HasConstraintName("FK_DOCUMENT_DOCUMENTPICTUREBLOB");
            entity.HasOne(d => d.Text).WithMany(p => p.Documents).HasForeignKey(d => d.Textid).HasConstraintName("FK_DOCUMENT_DOCUMENTTEXTBLOB");
        });

        modelBuilder.Entity<Documentaudiovideoblob>(entity =>
        {
            entity.ToTable("DOCUMENTAUDIOVIDEOBLOB");
            entity.Property(e => e.Id).HasDefaultValue(Guid.NewGuid()).HasColumnName("ID");
            entity.Property(e => e.Content).HasColumnName("CONTENT");
        });

        modelBuilder.Entity<Documentaudiovideohistoryblob>(entity =>
        {
            entity.ToTable("DOCUMENTAUDIOVIDEOHISTORYBLOB");
            entity.Property(e => e.Id).HasDefaultValue(Guid.NewGuid()).HasColumnName("ID");
            entity.Property(e => e.Content).HasColumnName("CONTENT");
        });

        modelBuilder.Entity<Documentauthor>(entity =>
        {
            entity.HasNoKey().ToTable("DOCUMENTAUTHOR");
            entity.Property(e => e.Authorid).HasColumnName("AUTHORID");
            entity.Property(e => e.Description).HasMaxLength(4000).IsUnicode(false).HasDefaultValue(null).HasColumnName("DESCRIPTION");
            entity.Property(e => e.Documentid).HasColumnName("DOCUMENTID");
            entity.HasOne(d => d.Author).WithMany().HasForeignKey(d => d.Authorid).OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_DOCUMENTAUTHOR_ENTITY");
            entity.HasOne(d => d.Document).WithMany().HasForeignKey(d => d.Documentid).OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_DOCUMENTAUTHOR_DOCUMENT");
        });

        modelBuilder.Entity<Documentdiversblob>(entity =>
        {
            entity.ToTable("DOCUMENTDIVERSBLOB");
            entity.Property(e => e.Id).HasDefaultValue(Guid.NewGuid()).HasColumnName("ID");
            entity.Property(e => e.Content).HasColumnName("CONTENT");
        });

        modelBuilder.Entity<Documentdivershistoryblob>(entity =>
        {
            entity.ToTable("DOCUMENTDIVERSHISTORYBLOB");
            entity.Property(e => e.Id).HasDefaultValue(Guid.NewGuid()).HasColumnName("ID");
            entity.Property(e => e.Content).HasColumnName("CONTENT");
        });

        modelBuilder.Entity<Documenthistory>(entity =>
        {
            entity.ToTable("DOCUMENTHISTORY");
            entity.Property(e => e.Id).HasDefaultValue(Guid.NewGuid()).HasColumnName("ID");
            entity.Property(e => e.Audiovideoid).HasDefaultValue(null).HasColumnName("AUDIOVIDEOID");
            entity.Property(e => e.Creationdate).HasDefaultValue(DateTime.Now).HasColumnName("CREATIONDATE");
            entity.Property(e => e.Description).HasMaxLength(4000).IsUnicode(false).HasDefaultValue(null).HasColumnName("DESCRIPTION");
            entity.Property(e => e.Diversid).HasDefaultValue(null).HasColumnName("DIVERSID");
            entity.Property(e => e.Documentid).HasColumnName("DOCUMENTID");
            entity.Property(e => e.Format).HasMaxLength(10).IsUnicode(false).HasDefaultValue(null).HasColumnName("FORMAT");
            entity.Property(e => e.Historytype).HasMaxLength(20).IsUnicode(false).HasDefaultValue("History").HasColumnName("HISTORYTYPE");
            entity.Property(e => e.Label).HasMaxLength(255).IsUnicode(false).HasDefaultValue(null).HasColumnName("LABEL");
            entity.Property(e => e.Pictureid).HasDefaultValue(null).HasColumnName("PICTUREID");
            entity.Property(e => e.Stockage).HasMaxLength(4000).IsUnicode(false).HasDefaultValue(null).HasColumnName("STOCKAGE");
            entity.Property(e => e.Textid).HasDefaultValue(null).HasColumnName("TEXTID");
            entity.Property(e => e.Version).HasMaxLength(20).IsUnicode(false).HasDefaultValue(null).HasColumnName("VERSION");
            entity.HasOne(d => d.Audiovideo).WithMany(p => p.Documenthistories).HasForeignKey(d => d.Audiovideoid).HasConstraintName("FK_DOCUMENT_DOCUMENTAUDIOVIDEOHISTORYBLOB");
            entity.HasOne(d => d.Divers).WithMany(p => p.Documenthistories).HasForeignKey(d => d.Diversid).HasConstraintName("FK_DOCUMENT_DOCUMENTDIVERSHISTORYBLOB");
            entity.HasOne(d => d.Document).WithMany(p => p.Documenthistories).HasForeignKey(d => d.Documentid).OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_DOCUMENTHISTORY_DOCUMENT");
            entity.HasOne(d => d.Picture).WithMany(p => p.Documenthistories).HasForeignKey(d => d.Pictureid).HasConstraintName("FK_DOCUMENT_DOCUMENTPICTUREHISTORYBLOB");
            entity.HasOne(d => d.Text).WithMany(p => p.Documenthistories).HasForeignKey(d => d.Textid).HasConstraintName("FK_DOCUMENT_DOCUMENTTEXTHISTORYBLOB");
        });

        modelBuilder.Entity<Documentpictureblob>(entity =>
        {
            entity.ToTable("DOCUMENTPICTUREBLOB");
            entity.Property(e => e.Id).HasDefaultValue(Guid.NewGuid()).HasColumnName("ID");
            entity.Property(e => e.Content).HasColumnName("CONTENT");
        });

        modelBuilder.Entity<Documentpicturehistoryblob>(entity =>
        {
            entity.ToTable("DOCUMENTPICTUREHISTORYBLOB");
            entity.Property(e => e.Id).HasDefaultValue(Guid.NewGuid()).HasColumnName("ID");
            entity.Property(e => e.Content).HasColumnName("CONTENT");
        });

        modelBuilder.Entity<Documenttag>(entity =>
        {
            entity.HasNoKey().ToTable("DOCUMENTTAG");
            entity.Property(e => e.Documentid).HasColumnName("DOCUMENTID");
            entity.Property(e => e.Tagid).HasColumnName("TAGID");
            entity.HasOne(d => d.Document).WithMany().HasForeignKey(d => d.Documentid).OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_DOCUMENTTAGE_DOCUMENT");
            entity.HasOne(d => d.Tag).WithMany().HasForeignKey(d => d.Tagid).OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_DOCUMENTTAG_ENTITY");
        });

        modelBuilder.Entity<Documenttextblob>(entity =>
        {
            entity.ToTable("DOCUMENTTEXTBLOB");
            entity.Property(e => e.Id).HasDefaultValue(Guid.NewGuid()).HasColumnName("ID");
            entity.Property(e => e.Content).HasColumnName("CONTENT");
        });

        modelBuilder.Entity<Documenttexthistoryblob>(entity =>
        {
            entity.ToTable("DOCUMENTTEXTHISTORYBLOB");
            entity.Property(e => e.Id).HasDefaultValue(Guid.NewGuid()).HasColumnName("ID");
            entity.Property(e => e.Content).HasColumnName("CONTENT");
        });

        modelBuilder.Entity<Documentvalidation>(entity =>
        {
            entity.ToTable("DOCUMENTVALIDATION");
            entity.Property(e => e.Id).HasDefaultValue(Guid.NewGuid()).HasColumnName("ID");
            entity.Property(e => e.Creationdate).HasDefaultValue(DateTime.Now).HasColumnName("CREATIONDATE");
            entity.Property(e => e.Documenthistoryid).HasDefaultValue(null).HasColumnName("DOCUMENTHISTORYID");
            entity.Property(e => e.Documentid).HasDefaultValue(null).HasColumnName("DOCUMENTID");
            entity.Property(e => e.Type).HasMaxLength(50).IsUnicode(false).HasColumnName("TYPE");
            entity.Property(e => e.Updatedate).HasColumnName("UPDATEDATE");
            entity.Property(e => e.Validateurid).HasColumnName("VALIDATEURID");
            entity.HasOne(d => d.Documenthistory).WithMany(p => p.Documentvalidations).HasForeignKey(d => d.Documenthistoryid).HasConstraintName("FK_DOCUMENTVALIDATION_DOCUMENTHISTORY");
            entity.HasOne(d => d.Document).WithMany(p => p.Documentvalidations).HasForeignKey(d => d.Documentid).HasConstraintName("FK_DOCUMENTVALIDATION_DOCUMENT");
            entity.HasOne(d => d.Validateur).WithMany(p => p.Documentvalidations).HasForeignKey(d => d.Validateurid).OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_DOCUMENTVALIDATION_ENTITY");
        });

        modelBuilder.Entity<Entity>(entity =>
        {
            entity.ToTable("ENTITY");
            entity.HasIndex(e => e.Code, "C1_ENTITY_UNIQUECODE").IsUnique();
            entity.HasIndex(e => e.Mail, "C2_ENTITY_UNIQUEMAIL").IsUnique();
            entity.HasIndex(e => e.Pictureblobid, "C3_ENTITY_PICTUREBLOB").IsUnique();
            entity.Property(e => e.Id).HasDefaultValue(Guid.NewGuid()).HasColumnName("ID");
            entity.Property(e => e.Code).HasMaxLength(4).IsUnicode(false).HasColumnName("CODE");
            entity.Property(e => e.Creationdate).HasDefaultValue(DateTime.Now).HasColumnName("CREATIONDATE");
            entity.Property(e => e.Familyname).HasMaxLength(30).IsUnicode(false).HasDefaultValue(null).HasColumnName("FAMILYNAME");
            entity.Property(e => e.Isemployee).HasDefaultValue(true).HasColumnName("ISEMPLOYEE");
            entity.Property(e => e.Istrainer).HasDefaultValue(false).HasColumnName("ISTRAINER");
            entity.Property(e => e.Mail).HasMaxLength(100).IsUnicode(false).HasColumnName("MAIL");
            entity.Property(e => e.Name).HasMaxLength(30).IsUnicode(false).HasColumnName("NAME");
            entity.Property(e => e.Phone).HasMaxLength(20).IsUnicode(false).HasDefaultValue(null).HasColumnName("PHONE");
            entity.Property(e => e.Pictureblobid).HasDefaultValue(null).HasColumnName("PICTUREBLOBID");
            entity.Property(e => e.Updatedate).HasDefaultValue(null).HasColumnType("datetime").HasColumnName("UPDATEDATE");
            entity.HasOne(d => d.Pictureblob).WithOne(p => p.Entity).HasForeignKey<Entity>(d => d.Pictureblobid).HasConstraintName("FK_ENTITY_PICTUREBLOB");
        });

        modelBuilder.Entity<Folder>(entity =>
        {
            entity.ToTable("FOLDER");
            entity.HasIndex(e => new { e.Repositoryid, e.Parentid, e.Name }, "C1_FOLDER_UNIQUENAMEINSAMEFOLDER").IsUnique();
            entity.Property(e => e.Id).HasDefaultValue(Guid.NewGuid()).HasColumnName("ID");
            entity.Property(e => e.Name).HasMaxLength(50).IsUnicode(false).HasColumnName("NAME");
            entity.Property(e => e.Parentid).HasDefaultValue(null).HasColumnName("PARENTID");
            entity.Property(e => e.Repositoryid).HasColumnName("REPOSITORYID");
            entity.HasOne(d => d.Parent).WithMany(p => p.InverseParent).HasForeignKey(d => d.Parentid).HasConstraintName("FK_FODLER_FOLDER");
            entity.HasOne(d => d.Repository).WithMany(p => p.Folders).HasForeignKey(d => d.Repositoryid).OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_FOLDER_REPOSITORY");
        });

        modelBuilder.Entity<Pictureblob>(entity =>
        {
            entity.ToTable("PICTUREBLOB");
            entity.Property(e => e.Id).HasDefaultValue(Guid.NewGuid()).HasColumnName("ID");
            entity.Property(e => e.Content).HasColumnName("CONTENT");
            entity.Property(e => e.Creationdate).HasDefaultValue(DateTime.Now).HasColumnName("CREATIONDATE");
        });

        modelBuilder.Entity<Repository>(entity =>
        {
            entity.ToTable("REPOSITORY");
            entity.HasIndex(e => e.Code, "C1_REPOSITORY_UNIQUECODE").IsUnique();
            entity.Property(e => e.Id).HasDefaultValue(Guid.NewGuid()).HasColumnName("ID");
            entity.Property(e => e.Code).HasMaxLength(50).IsUnicode(false).HasColumnName("CODE");
        });

        modelBuilder.Entity<Tag>(entity =>
        {
            entity.ToTable("TAG");
            entity.HasIndex(e => e.Code, "C1_TAG_UNIQUECODE").IsUnique();
            entity.Property(e => e.Id).HasDefaultValue(Guid.NewGuid()).HasColumnName("ID");
            entity.Property(e => e.Code).HasMaxLength(10).IsUnicode(false).HasColumnName("CODE");
            entity.Property(e => e.Creationdate).HasDefaultValue(DateTime.Now).HasColumnName("CREATIONDATE");
            entity.Property(e => e.Label).HasMaxLength(255).IsUnicode(false).HasDefaultValue(null).HasColumnName("LABEL");
            entity.Property(e => e.Updatedate).HasColumnName("UPDATEDATE");
        });
        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
