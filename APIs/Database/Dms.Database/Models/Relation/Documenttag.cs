﻿namespace Dms.Database;

public partial class Documenttag
{
    public Guid Documentid { get; set; }

    public Guid Tagid { get; set; }

    public virtual Document Document { get; set; } = null!;

    public virtual Tag Tag { get; set; } = null!;
}
