﻿namespace Dms.Database;

public partial class Documentauthor
{
    public Guid Authorid { get; set; }

    public Guid Documentid { get; set; }

    public string? Description { get; set; }

    public virtual Entity Author { get; set; } = null!;

    public virtual Document Document { get; set; } = null!;
}
