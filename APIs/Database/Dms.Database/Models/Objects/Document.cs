﻿namespace Dms.Database;

public partial class Document
{
    public Guid Id { get; set; }

    public string Label { get; set; } = null!;

    public string? Description { get; set; }

    public string Type { get; set; } = null!;

    public string? Format { get; set; }

    public Guid? Textid { get; set; }

    public Guid? Diversid { get; set; }

    public Guid? Pictureid { get; set; }

    public Guid? Audiovideoid { get; set; }

    public DateTime? Creationdate { get; set; }

    public DateTime? Updatedate { get; set; }

    public string? Version { get; set; }

    public bool Isinvalid { get; set; }

    public Guid Folderid { get; set; }

    public string? Stockage { get; set; }

    public virtual Documentaudiovideoblob? Audiovideo { get; set; }

    public virtual Documentdiversblob? Divers { get; set; }

    public virtual ICollection<Documenthistory> Documenthistories { get; set; } = new List<Documenthistory>();

    public virtual ICollection<Documentvalidation> Documentvalidations { get; set; } = new List<Documentvalidation>();

    public virtual Folder Folder { get; set; } = null!;

    public virtual Documentpictureblob? Picture { get; set; }

    public virtual Documenttextblob? Text { get; set; }
}
