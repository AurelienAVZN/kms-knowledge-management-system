﻿namespace Dms.Database;

public partial class Tag
{
    public Guid Id { get; set; }

    public string Code { get; set; } = null!;

    public string? Label { get; set; }

    public DateTime Creationdate { get; set; }

    public DateTime Updatedate { get; set; }
}
