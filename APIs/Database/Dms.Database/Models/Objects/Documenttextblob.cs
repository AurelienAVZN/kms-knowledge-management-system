﻿namespace Dms.Database;
public partial class Documenttextblob
{
    public Guid Id { get; set; }

    public byte[] Content { get; set; } = null!;

    public virtual ICollection<Document> Documents { get; set; } = new List<Document>();
}
