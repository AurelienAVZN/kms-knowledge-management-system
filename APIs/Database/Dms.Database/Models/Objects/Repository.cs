﻿namespace Dms.Database;

public partial class Repository
{
    public Guid Id { get; set; }

    public string Code { get; set; } = null!;

    public virtual ICollection<Folder> Folders { get; set; } = new List<Folder>();
}
