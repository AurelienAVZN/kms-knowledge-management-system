﻿namespace Dms.Database;

public partial class Folder
{
    public Guid Id { get; set; }

    public Guid? Parentid { get; set; }

    public Guid Repositoryid { get; set; }

    public string Name { get; set; } = null!;

    public virtual ICollection<Document> Documents { get; set; } = new List<Document>();

    public virtual ICollection<Folder> InverseParent { get; set; } = new List<Folder>();

    public virtual Folder? Parent { get; set; }

    public virtual Repository Repository { get; set; } = null!;
}
