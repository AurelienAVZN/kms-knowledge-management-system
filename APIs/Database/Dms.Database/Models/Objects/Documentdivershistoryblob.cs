﻿namespace Dms.Database;

public partial class Documentdivershistoryblob
{
    public Guid Id { get; set; }

    public byte[] Content { get; set; } = null!;

    public virtual ICollection<Documenthistory> Documenthistories { get; set; } = new List<Documenthistory>();
}
