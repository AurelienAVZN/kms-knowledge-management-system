﻿namespace Dms.Database;

public partial class Documentvalidation
{
    public Guid Id { get; set; }

    public Guid? Documentid { get; set; }

    public Guid? Documenthistoryid { get; set; }

    public DateTime Creationdate { get; set; }

    public DateTime? Updatedate { get; set; }

    public string Type { get; set; } = null!;

    public Guid Validateurid { get; set; }

    public virtual Document? Document { get; set; }

    public virtual Documenthistory? Documenthistory { get; set; }

    public virtual Entity Validateur { get; set; } = null!;
}
