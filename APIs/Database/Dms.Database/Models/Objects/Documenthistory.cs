﻿namespace Dms.Database;

public partial class Documenthistory
{
    public Guid Id { get; set; }

    public Guid Documentid { get; set; }

    public string? Label { get; set; }

    public string? Description { get; set; }

    public string? Format { get; set; }

    public Guid? Textid { get; set; }

    public Guid? Diversid { get; set; }

    public Guid? Pictureid { get; set; }

    public Guid? Audiovideoid { get; set; }

    public DateTime? Creationdate { get; set; }

    public string? Version { get; set; }

    public string? Stockage { get; set; }

    public string? Historytype { get; set; }

    public virtual Documentaudiovideohistoryblob? Audiovideo { get; set; }

    public virtual Documentdivershistoryblob? Divers { get; set; }

    public virtual Document Document { get; set; } = null!;

    public virtual ICollection<Documentvalidation> Documentvalidations { get; set; } = new List<Documentvalidation>();

    public virtual Documentpicturehistoryblob? Picture { get; set; }

    public virtual Documenttexthistoryblob? Text { get; set; }
}
