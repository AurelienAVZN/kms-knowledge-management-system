﻿namespace Dms.Database;

public partial class Pictureblob
{
    public Guid Id { get; set; }

    public byte[] Content { get; set; } = null!;

    public DateTime? Creationdate { get; set; }

    public virtual Entity? Entity { get; set; }
}
