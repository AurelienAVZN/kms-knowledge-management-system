﻿namespace Dms.Core;

/// <summary>
/// Give the list of available type of document in the Document Management System
/// </summary>
public enum DocumentType
{
    Divers = 0,
    Text = 1,
    Video = 2,
    Audio = 3,
    Picture = 4
}