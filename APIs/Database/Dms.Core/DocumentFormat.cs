﻿namespace Dms.Core;

/// <summary>
/// All the format take in charge by the different Api of the solution
/// </summary>
public enum DocumentFormat
{
    Unknown = 0,
    Pdf = 1,
    Txt = 2,
    Xml = 3,
    Html = 4,
    Mp3 = 5,
    Wav = 6,
    Mp4 = 7,
    Ogg = 8,
    Avi = 9,
    Png = 10,
    Jpeg = 11,
    Gif = 12
}