create extension IF NOT EXISTS "uuid-ossp" schema pg_catalog version "1.1";

create table PICTUREBLOB(
	"ID" uuid default uuid_generate_v4(),
	"PICTUREBLOB" bytea not null,
	"CREATIONDATE" timestamp default current_timestamp,
	primary key ("ID")
);

create table ENTITY(
	"ID" uuid default UUID_GENERATE_V4(),
	"CODE" varchar(4) not null,
	"NAME" varchar(50) not null,
	"FAMILYNAME" varchar(50) not null,
	"MAIL" varchar(100) not null,
	"PHONE" varchar(20) default null,
	"PICTUREBLOBID" uuid null,
	"UPDATEDATE" timestamp null,
	"CREATEDATE" timestamp not null default current_timestamp,
	"ISEMPLOYEE" int default 1,
	"ISTRAINER" int default 0,
	primary key ("ID"),
	constraint FK_ENTITY_PICTUREBLOB foreign key ("PICTUREBLOBID") references PICTUREBLOB("ID"),
	constraint C1_ENTITY_UNIQUECODE unique("CODE"),
	CONSTRAINT C2_ENTITY_UNIQUEMAIL UNIQUE ("MAIL"),
	CONSTRAINT C3_ENTITY_PICTUREBLOB UNIQUE ("PICTUREBLOBID"),
	constraint CP1_ENTITY_ISTRAINER check ("ISTRAINER" between 0 and 1),
	constraint CP2_ENTITY_ISEMPLOYEE check ("ISEMPLOYEE" between 0 and 1)
);

create table JOB(
	"ID" uuid default UUID_GENERATE_V4(),
	"CODE" varchar(20) not null,
	"LABEL" varchar(255) null,
	"DESCRIPTION" varchar(4000) null,
	"UPDATEDATE" timestamp null,
	"CREATIONDATE" timestamp default current_timestamp,
	"STARTVALIDITYDATE" timestamp default current_timestamp,
	"ENDVALIDITYDATE" timestamp not null,
	primary key ("ID"),
	constraint C1_JOB_UNIQUECODE unique("CODE")
);

create table SERVICE(
	"ID" uuid default UUID_GENERATE_V4(),
	"CODE" varchar(20) not null,
	"LABEL" varchar(255) null,
	"DESCRIPTION" varchar(4000) null,
	"UPDATEDATE" timestamp null,
	"CREATIONDATE" timestamp default current_timestamp,
	"STARTVALIDITYDATE" timestamp default current_timestamp,
	"ENDVALIDITYDATE" timestamp not null,
	primary key ("ID"),
	constraint C1_SERVICE_UNIQUECODE unique("CODE")
);

create table SKILL(
	"ID" uuid default UUID_GENERATE_V4(),
	"CODE" varchar(20) not null,
	"LABEL" varchar(255) null,
	"DESCRIPTION" varchar(4000) null,
	"UPDATEDATE" timestamp null,
	"CREATIONDATE" timestamp default current_timestamp,
	primary key ("ID"),
	unique("CODE")
);

create table KNOWLEDGE(
	"ID" uuid default UUID_GENERATE_V4(),
	"CODE" varchar(20) not null,
	"LABEL" varchar(255) null,
	"DESCRIPTION" varchar(4000) null,
	"UPDATEDATE" timestamp null,
	"CREATIONDATE" timestamp default current_timestamp,
	primary key ("ID"),
	unique("CODE")
);

--TABLE DE RELATION

create table EMPLOYEEJOB(
	"EMPLOYEEID" uuid not null,
	"JOBID" uuid not null,
	"STARTDATE" timestamp default current_timestamp,
	"ENDDATE" timestamp null,
	foreign key ("EMPLOYEEID") references EMPLOYEE("ID"),
	foreign key ("JOBID") references JOB("ID")
);

create table JOBSERVICE(
	"JOBID" uuid not null,
	"SERVICEID" uuid not null,
	"STARTDATE" timestamp default current_timestamp,
	"ENDDATE" timestamp null,
	foreign key ("SERVICEID") references SERVICE("ID"),
	foreign key ("JOBID") references JOB("ID")
);

create table EMPLOYEESERVICE(
	"EMPLOYEEID" uuid not null,
	"SERVICEID" uuid not null,
	"STARTDATE" timestamp default current_timestamp,
	"ENDDATE" timestamp null,
	foreign key ("SERVICEID") references SERVICE("ID"),
	foreign key ("EMPLOYEEID") references EMPLOYEE("ID")
);

create table JOBKNOWLEDGE(
	"JOBID" uuid not null,
	"KNOWLEDGEID" uuid not null,
	"STARTDATE" timestamp default current_timestamp,
	"ENDDATE" timestamp null,
	foreign key ("JOBID") references JOB("ID"),
	foreign key ("KNOWLEDGEID") references KNOWLEDGE("ID")
);

create table EMPLOYEEKNWOLEDGE(
	"EMPLOYEEID" uuid not null,
	"KNOWLEDGEID" uuid not null,
	"LEVEL" int not null default 1,
	"STARTDATE" timestamp default current_timestamp,
	"ENDDATE" timestamp null,
	foreign key ("EMPLOYEEID") references EMPLOYEE("ID"),
	foreign key ("KNOWLEDGEID") references KNOWLEDGE("ID"),
	constraint C1_EMPLOYEEKNOWLEDGE_LEVEL check ("LEVEL" between 1 and 5)
);

create table EMPLOYEESKILL(
	"EMPLOYEEID" uuid not null,
	"SKILLID" uuid not null,
	"LEVEL" int not null default 1,
	"STARTDATE" timestamp default current_timestamp,
	"ENDDATE" timestamp null,
	foreign key ("EMPLOYEEID") references EMPLOYEE("ID"),
	foreign key ("SKILLID") references SKILL("ID"),
	constraint C1_EMPLOYEESKILL_LEVEL check ("LEVEL" between 1 and 5)
);

create table JOBSKILL(
	"JOBID" uuid not null,
	"SKILLID" uuid not null,
	"STARTDATE" timestamp default current_timestamp,
	"ENDDATE" timestamp null,
	foreign key ("JOBID") references JOB("ID"),
	foreign key ("SKILLID") references SKILL("ID")
);

create table SKILLKNOWLEDGE(
	"KNOWLEDGEID" uuid not null,
	"SKILLID" uuid not null,
	"STARTDATE" timestamp default current_timestamp,
	"ENDDATE" timestamp null,
	foreign key ("KNOWLEDGEID") references KNOWLEDGE("ID"),
	foreign key ("SKILLID") references SKILL("ID")
);