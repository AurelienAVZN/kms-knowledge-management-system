--TRIG_SERVICE_ENDVALIDITY
create or replace function CHECK_VALIDITY_SERVICE()
returns trigger language plpgsql as $$
declare
	nbemployee int;
begin
	select count(*) into nbemployee from employeeservice where serviceid = new.serviceid;
	if nbemployee = 0 then
		update service set ENDVALIDITYDATE = current_timestamp where id = new.servideid;
	end if;
end;
$$

create or replace trigger TRIG_SERVICE_ENDVALIDITY 
after update or insert on employeeservice
for each statement
execute function CHECK_VALIDITY_SERVICE();

--TRIG_JOBSERVICE_ENDDATE
create or replace function END_JOBSERVICE_RELATION()
returns trigger language plpgsql as $$
begin
	update jobservice set enddate = current_timestamp where serviceid = new.id;
end;
$$

create or replace trigger TRIG_JOBSERVICE_ENDDATE 
after update on service
for each row
execute function END_JOBSERVICE_RELATION();

--TRIG_EMPLOYEEJOB_ENDDATE
create or replace function END_LAST_JOB()
returns trigger language plpgsql as $$
begin
	update employeejob set enddate = current_timestamp where jobid in 
	(select jobid from employeejob where enddate is null and employeeid = new.employeeid);
end;
$$ 

create or replace trigger TRIG_EMPLOYEEJOB_ENDDATE
before insert on employeejob
for each row
execute function END_LAST_JOB();

--TRIG_EMPLOYEE_ASCII
create or replace function NAME_FAMILYNAME_ASCII()
returns trigger as $NAME_FAMILYNAME_ASCII$
begin 
	new.ASCIINAME := to_ascii(new.name);
	new.asciifamilyname := to_ascii(new.familyname);
	return new;
end;
$NAME_FAMILYNAME_ASCII$ LANGUAGE plpgsql;

create or replace trigger TRIG_EMPLOYEE_ASCII
before insert or update on employee
for each row
execute function NAME_FAMILYNAME_ASCII();
