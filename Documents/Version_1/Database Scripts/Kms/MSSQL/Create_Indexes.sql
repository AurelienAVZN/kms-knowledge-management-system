CREATE INDEX ID1_ENTITY_NAME ON ENTITY(NAME);

CREATE INDEX ID1_EMPLOYEEJOBSERVICE_STARTDATE ON EMPLOYEEJOBSERVICE(STARTDATE);

CREATE INDEX ID1_EMPLOYEESKILL_LEVEL ON EMPLOYEESKILL(LEVEL);

CREATE INDEX ID1_EMPLOYEEAREA_LEVEL ON EMPLOYEEAREA(LEVEL);