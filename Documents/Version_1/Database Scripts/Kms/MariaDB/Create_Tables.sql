/*Pour les contraintes de cles primaires impossibles de définir un nom*/
create table PICTUREBLOB(
	ID UUID default UUID(),
	PICTUREBLOB LONGBLOB not null,
	constraint primary key(ID)
);

create table EMPLOYEE(
	ID UUID default UUID(),
	CODE varchar(4) not null,
	NAME varchar(50) not null,
	FAMILYNAME varchar(50) not null,
	PICTUREID UUID null,
	PHONE varchar(20) null,
	UPDATEDATE DATETIME null,
	CREATEDATE DATETIME default NOW(),
	ASCIINAME varchar(50) null,
	ASCIIFAMILYNAME varchar(50) null,
	constraint primary key (ID),
	constraint FK_EMPLOYEE_PICTURE foreign key (PICTUREID) references PICTUREBLOB(ID),
	constraint C1_EMPLOYEE_CODE unique (CODE)
);

create table ACCOUNT(
	ID UUID default UUID(),
	EMPLOYEEID UUID not null,
	UPDATEDATE DATETIME null,
	MAIL varchar(100) not null,
	TYPE varchar(20) default 'Employee',
	constraint primary key (ID),
	constraint FK_ACCOUNT_EMPLOYEE foreign key (EMPLOYEEID) references EMPLOYEE(ID),
	constraint C1_ACCOUNT_MAIL unique(MAIL)
);

create table SKILL(
	ID UUID default UUID(),
	CODE varchar(20) not null,
	LABEL varchar(255) null,
	DESCRIPTION varchar(4000) null,
	UPDATEDATE DATETIME null,
	CREATIONDATE DATETIME default NOW(),
	constraint primary key (ID),
	constraint C1_SKILL_CODE unique(CODE)
);

create table KNOWLEDGE(
	ID UUID default UUID(),
	CODE varchar(20) not null,
	LABEL varchar(255) null,
	DESCRIPTION varchar(4000) null,
	UPDATEDATE DATETIME null,
	CREATIONDATE DATETIME default NOW(),
	constraint primary key (ID),
	constraint C1_KNOWLEDGE_CODE unique(CODE)
);

create table JOB(
	ID UUID default UUID(),
	CODE varchar(20) not null,
	LABEL varchar(255) null,
	DESCRIPTION varchar(4000) null,
	UPDATEDATE DATETIME null,
	CREATIONDATE DATETIME default NOW(),
	STARTVALIDITYDATE DATETIME default NOW(),
	ENDVALIDITYDATE DATETIME not null,
	constraint primary key (ID),
	constraint C1_JOB_CODE unique(CODE)
);

create table SERVICE(
	ID UUID default UUID(),
	CODE varchar(20) not null,
	LABEL varchar(255) null,
	DESCRIPTION varchar(4000) null,
	UPDATEDATE DATETIME null,
	CREATIONDATE DATETIME default NOW(),
	STARTVALIDITYDATE DATETIME default NOW(),
	ENDVALIDITYDATE DATETIME not null,
	constraint primary key (ID),
	constraint C1_SERVICE_CODE unique(CODE)
);

/*TABLE DE RELATION*/
create table EMPLOYEEJOB(
	EMPLOYEEID UUID not null,
	JOBID UUID not null,
	STARTDATE DATETIME default NOW(),
	ENDDATE DATETIME null,
	constraint FK_EMPJOB_EMPLOYEE foreign key (EMPLOYEEID) references EMPLOYEE(ID),
	constraint FK_EMPJOB_JOB foreign key (JOBID) references JOB(ID)
);

create table JOBSERVICE(
	JOBID UUID not null,
	SERVICEID UUID not null,
	STARTDATE DATETIME default NOW(),
	ENDDATE DATETIME null,
	constraint FK_JOBSERVICE_SERVICE foreign key (SERVICEID) references SERVICE(ID),
	constraint FK_JOBSERVICE_JOB foreign key (JOBID) references JOB(ID)
);

create table EMPLOYEESERVICE(
	EMPLOYEEID UUID not null,
	SERVICEID UUID not null,
	STARTDATE DATETIME default NOW(),
	ENDDATE DATETIME null,
	constraint FK_EMPLOYEESERVICE_SERVICE foreign key (SERVICEID) references SERVICE(ID),
	constraint FK_EMPLOYEESERVICE_EMPLOYEE foreign key (EMPLOYEEID) references EMPLOYEE(ID)
);

create table JOBKNOWLEDGE(
	JOBID UUID not null,
	KNOWLEDGEID UUID not null,
	STARTDATE DATETIME default NOW(),
	ENDDATE DATETIME null,
	constraint FK_JOBKNOWLEDGE_JOB foreign key (JOBID) references JOB(ID),
	constraint FK_JOBKNOWLEDGE_KNOWLEDGE foreign key (KNOWLEDGEID) references KNOWLEDGE(ID)
);

create table EMPLOYEEKNWOLEDGE(
	EMPLOYEEID UUID not null,
	KNOWLEDGEID UUID not null,
	LEVEL INTEGER default 1,
	STARTDATE DATETIME default NOW(),
	ENDDATE DATETIME null,
	constraint FK_EMPLOYEEKNWOLEDGE_EMPLOYEE foreign key (EMPLOYEEID) references EMPLOYEE(ID),
	constraint FK_EMPLOYEEKNWOLEDGE_KNOWLEDGE foreign key (KNOWLEDGEID) references KNOWLEDGE(ID)
);

create table EMPLOYEESKILL(
	EMPLOYEEID UUID not null,
	SKILLID UUID not null,
	LEVEL INTEGER default 1,
	STARTDATE DATETIME default NOW(),
	ENDDATE DATETIME null,
	constraint FK_EMPLOYEESKILL_EMPLOYEE foreign key (EMPLOYEEID) references EMPLOYEE(ID),
	constraint FK_EMPLOYEESKILL_SKILL foreign key (SKILLID) references SKILL(ID)
);

create table JOBSKILL(
	JOBID UUID not null,
	SKILLID UUID not null,
	STARTDATE DATETIME default NOW(),
	ENDDATE DATETIME null,
	constraint FK_JOBSKILL_JOB foreign key (JOBID) references JOB(ID),
	constraint FK_JOBSKILL_SKILL foreign key (SKILLID) references SKILL(ID)
);

create table SKILLKNOWLEDGE(
	KNOWLEDGEID UUID not null,
	SKILLID UUID not null,
	STARTDATE DATETIME default NOW(),
	ENDDATE DATETIME null,
	constraint FK_SKILLKNOWLEDGE_KNOWLEDGE foreign key (KNOWLEDGEID) references KNOWLEDGE(ID),
	constraint FK_SKILLKNOWLEDGE_SKILL foreign key (SKILLID) references SKILL(ID)
);