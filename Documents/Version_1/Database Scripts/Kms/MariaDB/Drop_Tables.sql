/*TABLES PRINCIPALES*/
drop table if exists ACCOUNT;
drop table if exists EMPLOYEE;
drop table if exists PICTUREBLOB;
drop table if exists SKILL;
drop table if exists KNOWLEDGE;
drop table if exists JOB;
drop table if exists SERVICE;
/*TABLES DE RELATION*/
drop table if exists EMPLOYEEJOB;
drop table if exists JOBSERVICE;
drop table if exists EMPLOYEESERVICE;
drop table if exists JOBKNOWLEDGE;
drop table if exists EMPLOYEEKNWOLEDGE;
drop table if exists EMPLOYEESKILL;
drop table if exists JOBSKILL;
drop table if exists SKILLKNOWLEDGE;