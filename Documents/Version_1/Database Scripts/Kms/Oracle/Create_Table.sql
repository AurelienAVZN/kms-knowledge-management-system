CREATE TABLE PICTUREBLOB(
	"ID" RAW(16) default SYS_GUID(),
	"PICTUREBLOB" BLOB not null,
	primary key ("ID")
);

create table EMPLOYEE(
	"ID" RAW(16) default SYS_GUID(),
	"CODE" varchar(4) not null,
	"NAME" varchar(50) not null,
	"FAMILYNAME" varchar(50) not null,
	"PICTUREID" RAW(16) null,
	"PHONE" varchar(20) null,
	"UPDATEDATE" timestamp null,
	"CREATEDATE" timestamp default current_timestamp,
	"ASCIINAME" varchar(50) null,
	"ASCIIFAMILYNAME" varchar(50) null,
	primary key ("ID"),
	foreign key ("PICTUREID") references PICTUREBLOB("ID"),
	unique("CODE")
);

create table ACCOUNT(
	"ID" RAW(16) default SYS_GUID(),
	"EMPLOYEEID" RAW(16) not null,
	"UPDATEDATE" timestamp null,
	"MAIL" varchar(100) not null,
	"TYPE" varchar(20) default 'Employee',
	primary key ("ID"),
	foreign key ("EMPLOYEEID") references EMPLOYEE("ID"),
	unique("MAIL"),
	constraint C1_ACCOUNT_TYPE check ("TYPE" in ('Employee', 'Admin', 'Manager'))
);

create table SKILL(
	"ID" RAW(16) default SYS_GUID(),
	"CODE" varchar(20) not null,
	"LABEL" varchar(255) null,
	"DESCRIPTION" varchar(4000) null,
	"UPDATEDATE" timestamp null,
	"CREATIONDATE" timestamp default current_timestamp,
	primary key ("ID"),
	unique("CODE")
);

create table KNOWLEDGE(
	"ID" RAW(16) default SYS_GUID(),
	"CODE" varchar(20) not null,
	"LABEL" varchar(255) null,
	"DESCRIPTION" varchar(4000) null,
	"UPDATEDATE" timestamp null,
	"CREATIONDATE" timestamp default current_timestamp,
	primary key ("ID"),
	unique("CODE")
);

create table JOB(
	"ID" RAW(16) default SYS_GUID(),
	"CODE" varchar(20) not null,
	"LABEL" varchar(255) null,
	"DESCRIPTION" varchar(4000) null,
	"UPDATEDATE" timestamp null,
	"CREATIONDATE" timestamp default current_timestamp,
	"STARTVALIDITYDATE" timestamp default current_timestamp,
	"ENDVALIDITYDATE" timestamp not null,
	primary key ("ID"),
	unique("CODE")
);

create table SERVICE(
	"ID" RAW(16) default SYS_GUID(),
	"CODE" varchar(20) not null,
	"LABEL" varchar(255) null,
	"DESCRIPTION" varchar(4000) null,
	"UPDATEDATE" timestamp null,
	"CREATIONDATE" timestamp default current_timestamp,
	"STARTVALIDITYDATE" timestamp default current_timestamp,
	"ENDVALIDITYDATE" timestamp not null,
	primary key ("ID"),
	unique("CODE")
);

--TABLE DE RELATION

create table EMPLOYEEJOB(
	"EMPLOYEEID" RAW(16) not null,
	"JOBID" RAW(16) not null,
	"STARTDATE" timestamp default current_timestamp,
	"ENDDATE" timestamp null,
	foreign key ("EMPLOYEEID") references EMPLOYEE("ID"),
	foreign key ("JOBID") references JOB("ID")
);

create table JOBSERVICE(
	"JOBID" RAW(16) not null,
	"SERVICEID" RAW(16) not null,
	"STARTDATE" timestamp default current_timestamp,
	"ENDDATE" timestamp null,
	foreign key ("SERVICEID") references SERVICE("ID"),
	foreign key ("JOBID") references JOB("ID")
);

create table EMPLOYEESERVICE(
	"EMPLOYEEID" RAW(16) not null,
	"SERVICEID" RAW(16) not null,
	"STARTDATE" timestamp default current_timestamp,
	"ENDDATE" timestamp null,
	foreign key ("SERVICEID") references SERVICE("ID"),
	foreign key ("EMPLOYEEID") references EMPLOYEE("ID")
);

create table JOBKNOWLEDGE(
	"JOBID" RAW(16) not null,
	"KNOWLEDGEID" RAW(16) not null,
	"STARTDATE" timestamp default current_timestamp,
	"ENDDATE" timestamp null,
	foreign key ("JOBID") references JOB("ID"),
	foreign key ("KNOWLEDGEID") references KNOWLEDGE("ID")
);

create table EMPLOYEEKNWOLEDGE(
	"EMPLOYEEID" RAW(16) not null,
	"KNOWLEDGEID" RAW(16) not null,
	"LEVEL" NUMBER(1) default 1,
	"STARTDATE" timestamp default current_timestamp,
	"ENDDATE" timestamp null,
	foreign key ("EMPLOYEEID") references EMPLOYEE("ID"),
	foreign key ("KNOWLEDGEID") references KNOWLEDGE("ID"),
	constraint C1_EMPLOYEEKNOWLEDGE_LEVEL check ("LEVEL" between 1 and 5)
);

create table EMPLOYEESKILL(
	"EMPLOYEEID" RAW(16) not null,
	"SKILLID" RAW(16) not null,
	"LEVEL" NUMBER(1) default 1,
	"STARTDATE" timestamp default current_timestamp,
	"ENDDATE" timestamp null,
	foreign key ("EMPLOYEEID") references EMPLOYEE("ID"),
	foreign key ("SKILLID") references SKILL("ID"),
	constraint C1_EMPLOYEESKILL_LEVEL check ("LEVEL" between 1 and 5)
);

create table JOBSKILL(
	"JOBID" RAW(16) not null,
	"SKILLID" RAW(16) not null,
	"STARTDATE" timestamp default current_timestamp,
	"ENDDATE" timestamp null,
	foreign key ("JOBID") references JOB("ID"),
	foreign key ("SKILLID") references SKILL("ID")
);

create table SKILLKNOWLEDGE(
	"KNOWLEDGEID" RAW(16) not null,
	"SKILLID" RAW(16) not null,
	"STARTDATE" timestamp default current_timestamp,
	"ENDDATE" timestamp null,
	foreign key ("KNOWLEDGEID") references KNOWLEDGE("ID"),
	foreign key ("SKILLID") references SKILL("ID")
);